import {createStore, combineReducers, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

// Importing Reducers
import main from './reducers/mainReducer';
import tasks from './reducers/tasksReducer';
import consultations from './reducers/consultationsReducer';
import doctorsCategories from './reducers/doctorsCategoriesReducer';
import employees from './reducers/employeesReducer';

export default createStore(
  combineReducers({
    main,
    tasks,
    consultations,
    doctorsCategories,
    employees
  }),
  {},
  applyMiddleware(logger, thunk, promise())
);
