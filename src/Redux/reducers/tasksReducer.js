import tasksState from '../initialStates/tasksState';

const tasksReducer = (state = tasksState, action) => {
  switch (action.type) {
    case 'CREATE_TASK':
    state = {
      ...state,
      tasks: action.payload
    };
    break;
    case 'UPDATE_TASK':
    state = {
      ...state,
      user: action.payload
    };
    break;
    case 'DELETE_TASK':
    state = {
      ...state,
      user: null
    };
    break;
    default:
    break;
  }
  return state;
};

export default tasksReducer;
