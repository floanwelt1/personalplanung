import employeesState from '../initialStates/employeesState';

const employeesReducer = (state = employeesState, action) => {
  switch (action.type) {
    case 'CREATE':
    state = {
      ...state,
      mainView: action.payload
    };
    break;
    case 'REMOVE':
    state = {
      ...state,
      user: action.payload
    };
    break;
    case 'UPDATE':
    state = {
      ...state,
      user: null
    };
    break;
    default:
    break;
  }
  return state;
};

export default employeesReducer;
