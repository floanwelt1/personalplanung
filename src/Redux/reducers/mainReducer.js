import mainState from '../initialStates/mainState';

const mainReducer = (state = mainState, action) => {
  switch (action.type) {
    case 'SET_MAIN_VIEW':
    state = {
      ...state,
      mainView: action.payload
    };
    break;
    case 'SET_USER':
    state = {
      ...state,
      user: action.payload
    };
    break;
    case 'LOGOUT_USER':
    state = {
      ...state,
      user: null
    };
    break;
    default:
    break;
  }
  return state;
};

export default mainReducer;
