export function setMainView(viewName) {
    return {
        type: "SET_MAIN_VIEW",
        payload: viewName
    };
}
