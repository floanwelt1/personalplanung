const doctorsCategoriesState = {
    key1: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key1',
            name: 'Anmeldung',
            description: '',
            adult: false,
            roomKeys: []
    },
    key2: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key2',
            name: 'Dr. Potter',
            description: '',
            adult: false,
            roomKeys: []
    },
    key3: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key3',
            name: 'Frau Hyde',
            description: '',
            adult: false,
            roomKeys: []
    },
    key4: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key4',
            name: 'Dr. Kuhlmann',
            description: '',
            adult: false,
            roomKeys: []
    },
    key5: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key5',
            name: 'Dr. Bauerbach',
            description: '',
            adult: false,
            roomKeys: []
    },
    key6: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key6',
            name: 'Frau Meir',
            description: '',
            adult: false,
            roomKeys: []
    },
    key7: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key7',
            name: 'Frau Hirte',
            description: '',
            adult: false,
            roomKeys: []
    },
    key8: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key8',
            name: 'Dr. Sauer',
            description: '',
            adult: false,
            roomKeys: []
    },
    key9: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key9',
            name: 'Nebenher',
            description: '',
            adult: false,
            roomKeys: []
    },
    key10: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key10',
            name: 'Telefonzentrale',
            description: '',
            adult: false,
            roomKeys: []
    },
    key11: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key1',
            name: 'Buero',
            description: '',
            adult: false,
            roomKeys: []
    },
    key12: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key2',
            name: 'Terminplaene',
            description: '',
            adult: false,
            roomKeys: []
    }
};

export default doctorsCategoriesState;
