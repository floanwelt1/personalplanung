const consultationsState = {
    key1: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key1',
            name: 'IVOM',
            description: '',
            adult: false,
            roomKeys: []
    },
    key2: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key2',
            name: 'CV',
            description: '',
            adult: false,
            roomKeys: []
    },
    key3: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key3',
            name: 'LASER LASER',
            description: '',
            adult: false,
            roomKeys: []
    },
    key4: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key4',
            name: 'BUERO',
            description: '',
            adult: false,
            roomKeys: []
    },
    key5: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key5',
            name: 'EXCIMER',
            description: '',
            adult: false,
            roomKeys: []
    },
    key6: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key6',
            name: 'GKV',
            description: '',
            adult: false,
            roomKeys: []
    },
    key7: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key7',
            name: 'OCT',
            description: '',
            adult: false,
            roomKeys: []
    },
    key8: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key8',
            name: 'Tropfer',
            description: '',
            adult: false,
            roomKeys: []
    },
    key9: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key9',
            name: 'Tropfer',
            description: '',
            adult: false,
            roomKeys: []
    },
    key10: {
            _id: 'abcdef',
            sortPosition: 1,
            type: null,
            key: 'key10',
            name: 'Tropfer',
            description: '',
            adult: false,
            roomKeys: []
    }
};

export default consultationsState;
