// Importing React
import React, { Component, Fragment } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

// Importing components

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});



class CreateTemplateDialog extends Component {
  constructor(props) {
      super(props);
      this.state = {
        direction: 'row',
        justify: 'space-around',
        alignItems: 'flex-start',
      };
    }

  handleChange = () => {
    return null;
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes, consultation } = this.props;

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth='lg'
      >
        <DialogTitle id="form-dialog-title">
          Wochenvorlage aus aktueller Woche erstellen
        </DialogTitle>
        <DialogContent>
            <DialogContentText>
              Bitte wählen Sie einen Namen fuer die Vorlage aus:
            </DialogContentText>
            <TextField
              id="outlined-number"
              label="Name Wochenvorlage"
              fullWidth
              value={this.state.name}
              onChange={this.handleChange('name')}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
              variant="outlined"
            />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleClose} color="primary">
            Abbrechen
          </Button>
          <Button onClick={this.props.handleClose} color="primary">
            Vorlage erstellen
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

CreateTemplateDialog.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(CreateTemplateDialog));
