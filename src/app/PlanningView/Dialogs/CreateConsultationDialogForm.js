// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { TimePicker } from 'material-ui-pickers';

// Importing components
import ConsultationsTasksTable from './ConsultationsTasksTable';
import AddTaskForm from './AddTaskForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit,
  },
  timePickerStart: {
    width: 'calc(50% - 8px)',
    marginTop: theme.spacing.unit * 2,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: theme.spacing.unit,
  },
  timePickerEnd: {
    width: 'calc(50% - 8px)',
    marginTop: theme.spacing.unit * 2,
    marginBottom: 0,
    marginLeft: theme.spacing.unit,
    marginRight: 0,

  }
});


class CreateConsultationDialogForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
      <form>
        <FormControl fullWidth variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Arzt/Kategorie
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="2" value="2">Dr. Potter</option>
            <option key="1" value="1">Dr. Hyde</option>
          </Select>
        </FormControl>
        <FormControl fullWidth variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Sprechstundenvorlage
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="1" value="1">IVOM</option>
            <option key="2" value="2">Laser</option>
            <option key="2" value="2">GKV</option>
          </Select>
        </FormControl>

        <TimePicker
          className={classes.timePickerStart}
          keyboard
          ampm={false}
          label="Startzeit"
          invalidDateMessage="Ungültige Eingabe"
          format="hh:mm"
          mask={[/\d/, /\d/, ':', /\d/, /\d/]}
          value={this.props.consultation.startTime}
          onChange={this.handleStartTime}
          variant="outlined"
        />
        <TimePicker
          className={classes.timePickerEnd}
          keyboard
          ampm={false}
          label="Endzeit"
          invalidDateMessage="Ungültige Eingabe"
          format="hh:mm"
          mask={[/\d/, /\d/, ':', /\d/, /\d/]}
          value={this.props.consultation.endTime}
          onChange={this.handleEndTime}
          variant="outlined"
        />
      </form>

      <ConsultationsTasksTable />
      </div>
    );
  }}

CreateConsultationDialogForm.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(CreateConsultationDialogForm));
