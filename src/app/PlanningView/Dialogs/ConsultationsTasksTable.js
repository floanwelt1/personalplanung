// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormLabel from '@material-ui/core/FormLabel';

import { TimePicker } from 'material-ui-pickers';

// Importing components
import TimeElement from './TimeElement';
import RoomSelector from './RoomSelector';

const styles = theme => ({
  table: {
    'margin-left': 'auto',
    'margin-right': 'auto',
    maxHeight: 350,
    overflow: 'auto',
  },
  buttonCell: {
    width: 88,
    padding: 0,
  },
  timePicker: {
    width: 110,
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
  },
  roomSelector: {
    width: 110,
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ConsultationsTasksTable extends Component {
  constructor(props) {
      super(props);
      this.state = {
        selectedTime: new Date(),
      };
    }

    handleDateChange = time => {
        this.setState({ selectedTime: time });
      };
  componentDidMount() {

    }

  createTableRow = (taskName, start = 0, end = 0) => {
    return(
      <TableRow>
        <TableCell className={this.props.classes.buttonCell}>
        <IconButton aria-label="Add">
          <AddIcon fontSize="small" />
        </IconButton>
          <IconButton aria-label="Delete">
            <DeleteIcon fontSize="small" />
          </IconButton>
        </TableCell>
        <TableCell align="center">
          {taskName}
        </TableCell>
        <TableCell align="center" className={this.props.classes.timePicker}>
          <TimeElement />
        </TableCell>
        <TableCell align="center" className={this.props.classes.timePicker}>
          <TimeElement />
        </TableCell>
        <TableCell align="center" className={this.props.classes.timePicker}>
          <TimeElement />
        </TableCell>
        <TableCell align="center" className={this.props.classes.timePicker}>
          <TimeElement />
        </TableCell>
        <TableCell align="center" className={this.props.classes.roomSelector}>
          <RoomSelector />
        </TableCell>
      </TableRow>
    )
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <Paper className={classes.table}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="none" align="left" className={classes.buttonCell}></TableCell>
            <TableCell align="center">Taetigkeit</TableCell>
            <TableCell align="center" colSpan={2}>Anfang/Ende</TableCell>
            <TableCell align="center" colSpan={2}>Pause</TableCell>
            <TableCell align="center">Raum</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {this.createTableRow("VU", "+30", "-30")}
            {this.createTableRow("CV", "+30", "-30")}
            {this.createTableRow("Schreiber", "0", "0")}
            {this.createTableRow("Zuschauer", "+60", "-30")}
            {this.createTableRow("Mitlaeufer", "+30", "-30")}
        </TableBody>
      </Table>
    </Paper>
  );
}}

ConsultationsTasksTable.propTypes = {classes: PropTypes.object.isRequired};
ConsultationsTasksTable = connect(mapStateToProps, mapDispatchToProps)(ConsultationsTasksTable);
export default (withStyles(styles)(ConsultationsTasksTable));
