// Importing React
import React, { Component, Fragment } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import TimePicker from 'material-ui-pickers/TimePicker';

// Importing components
import CreateConsultationDialogForm from './CreateConsultationDialogForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing.unit,
  },
  iconbutton: {
    float: 'right',
  },
  formControl: {
  margin: theme.spacing.unit,
  minWidth: 220,
  },
  select: {
    width: 250,
  },
  paper: {
    position: 'absolute',
    width: 1000,
    left: 200,
    top: 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
});



class CreateConsultationDialog extends Component {
  constructor(props) {
      super(props);
      this.state = {
        rooms: {
          vu1 :
          {
            _id: 'vu1',
            name: 'VU1'
          },
          vu2 :
          {
            _id: 'vu2',
            name: 'VU2'
          },
          vu3 :
          {
            _id: 'vu3',
            name: 'VU3'
          }
        },
        direction: 'row',
        justify: 'space-around',
        alignItems: 'flex-start',
      };
    }


//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//

  handleChangeModal = key => event => {
    let value = event.target.value;
    if (key === 'fullDay') {
      value = event.target.checked;
    }
    this.props.handleModalUpdate(key, value);
    };

  handleStartTime = (timestamp) => {
    let currentDate = this.props.currentDate.getDate();
    let currentMonth = this.props.currentDate.getMonth();
    let currentFullYear = this.props.currentDate.getFullYear();
    let timestampHours = timestamp.getHours();
    let timestampMinutes = timestamp.getMinutes();

    if (timestampHours < 6) {
      timestampHours = 6;
    } else if (timestampHours > 21 || (timestampHours === 21 && timestampMinutes > 30)) {
      timestampHours = 21;
      timestampMinutes = 30;
    }

    if (timestampMinutes > 0 && timestampMinutes < 8) {
      timestampMinutes = 0;
    } else if (timestampMinutes >= 8 && timestampMinutes < 15) {
      timestampMinutes = 15;
    } else if (timestampMinutes > 15 && timestampMinutes < 23) {
      timestampMinutes = 15;
    } else if (timestampMinutes >= 23 && timestampMinutes < 30) {
      timestampMinutes = 30;
    } else if (timestampMinutes > 30 && timestampMinutes < 38) {
      timestampMinutes = 30;
    } else if (timestampMinutes >= 38 && timestampMinutes < 45) {
      timestampMinutes = 45
    } else if (timestampMinutes > 45 && timestampMinutes < 53) {
      timestampMinutes = 45
    } else if (timestampMinutes >=53 && timestampMinutes < 60) {
      timestampMinutes = 0;
    } else if (timestampMinutes >= 60) {
      timestampMinutes = 0;
    }

    timestamp.setMilliseconds(0);
    timestamp.setSeconds(0);
    timestamp.setMinutes(timestampMinutes);
    timestamp.setHours(timestampHours);
    timestamp.setDate(currentDate);
    timestamp.setMonth(currentMonth);
    timestamp.setFullYear(currentFullYear);
    this.props.handleModalUpdate('startTime', timestamp);
  }

  handleEndTime = (timestamp) => {
    let currentDate = this.props.currentDate.getDate();
    let currentMonth = this.props.currentDate.getMonth();
    let currentFullYear = this.props.currentDate.getFullYear();
    let timestampHours = timestamp.getHours();
    let timestampMinutes = timestamp.getMinutes();

    if (timestampHours < 6) {
      timestampHours = 6;
    } else if (timestampHours > 21 || (timestampHours === 21 && timestampMinutes > 30)) {
      timestampHours = 21;
      timestampMinutes = 30;
    }
    if (timestampMinutes > 0 && timestampMinutes < 8) {
      timestampMinutes = 0;
    } else if (timestampMinutes >= 8 && timestampMinutes < 15) {
      timestampMinutes = 15;
    } else if (timestampMinutes > 15 && timestampMinutes < 23) {
      timestampMinutes = 15;
    } else if (timestampMinutes >= 23 && timestampMinutes < 30) {
      timestampMinutes = 30;
    } else if (timestampMinutes > 30 && timestampMinutes < 38) {
      timestampMinutes = 30;
    } else if (timestampMinutes >= 38 && timestampMinutes < 45) {
      timestampMinutes = 45
    } else if (timestampMinutes > 45 && timestampMinutes < 53) {
      timestampMinutes = 45
    } else if (timestampMinutes >=53 && timestampMinutes < 60) {
      timestampMinutes = 0;
    } else if (timestampMinutes >= 60) {
      timestampMinutes = 0;
    }
    timestamp.setMilliseconds(0);
    timestamp.setSeconds(0);
    timestamp.setMinutes(timestampMinutes);
    timestamp.setHours(timestampHours);
    timestamp.setDate(currentDate);
    timestamp.setMonth(currentMonth);
    timestamp.setFullYear(currentFullYear);
    this.props.handleModalUpdate('endTime', timestamp);
  }

//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

// UGLY AS FUCK!!!! ----> REPLACE AS SOON AS POSSIBLE!!!!
// PROBLEM IS REFERRING TO TASKS BY KEY PROPERTY INSTEAD OF // ID
// --> WHEN DYNAMICALLY CREATING TASKS --> MAP TASKS TO SERVICES BY DATABASE-ID
// INSTEAD OF KEY LIKE "recorder"
  findTask = (task) => {

    let keys = Object.keys(this.props.tasks);
    let taskName = "error";
    keys.forEach(key => {
      if (this.props.tasks[key].key === task) {
        taskName = this.props.tasks[key].name;
      }
    });
    return (taskName);
  }

//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

  renderDoctor = (consultation, keysDoctors) => {
    if (consultation.consultationClassId !== '5b5909d2e7179a264f0a2127' &&
        consultation.consultationClassId !== '5b590948e7179a264f0a2085' &&
        consultation.consultationClassId !== '5b748f65e7179a09abb611ce' &&
        consultation.consultationClassId !== '5b748f7ae7179a09abb611d9' &&
        consultation.consultationClassId !== '5b748f9ae7179a09abb611fa') {
      let index = keysDoctors.indexOf('5b4c02c9fb6fc062bcfcffbd');
      if (index !== -1) keysDoctors.splice(index, 1);
      index = keysDoctors.indexOf('5b4c02f9fb6fc062bcfcffd4');
      if (index !== -1) keysDoctors.splice(index, 1);
      return (
        <FormControl >
          <InputLabel htmlFor="doctor-select">Arzt</InputLabel>
          <Select
            native
            value={consultation.doctor}
            onChange={this.handleChangeModal('doctor')}
            inputProps={{
              name: 'doctor',
              id: 'doctor-select',
            }}
          >
            <option key="0" value="" />
            {keysDoctors.map((id) => (this.returnDoctorOptions(id)))}
          </Select>
        </FormControl>
      )
    }
  }

  renderSelectTime = (consultation) => {
    if (consultation.consultationClassId !== '5b590948e7179a264f0a2085') {
    return (
      <Fragment>

        <TimePicker
        keyboard
        ampm={false}
        label="Startzeit"
        invalidDateMessage="Ungültige Eingabe"
        mask={[/\d/, /\d/, ':', /\d/, /\d/]} value={consultation.startTime}
        onChange={this.handleStartTime} />

        <TimePicker keyboard ampm={false} label="Endzeit"
        invalidDateMessage="Ungültige Eingabe"
        mask={[/\d/, /\d/, ':', /\d/, /\d/]} value={consultation.endTime}
        onChange={this.handleEndTime}
        />
    </Fragment>
  )
    }
  }

  returnConsultationOptions = (consultation_id) => {
    let consultation = this.props.consultations[consultation_id];
    return (
        <option key={consultation._id} value={consultation._id}>{consultation.name}</option>
      )
    };

  returnDoctorOptions = (id) => {
    let doctor = this.props.doctors[id];
    return (
      <option key={doctor._id} value={doctor._id}>{doctor.gender + ' '+
      doctor.title + ' ' + doctor.firstName + ' ' + doctor.lastName}</option>
      )
    };

//-----------------------------TO_DELETE_AFTER_DEBUGGING--------------------//
//--------------------------------------------------------------------------//



//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes, consultation } = this.props;

    //let keysConsultations = Object.keys(this.props.consultations);
    //let keysDoctors = Object.keys(this.props.doctors);

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth='lg'
      >
        <DialogTitle id="form-dialog-title">
          {this.props.new ? "Neue Sprechstunde erstellen" : "Sprechstunde bearbeiten"}
        </DialogTitle>
        <DialogContent>
            <DialogContentText>
              Bitte wählen Sie die gewünschte Sprechstunde aus:
            </DialogContentText>
          <CreateConsultationDialogForm consultation={{startTime: "11:30", endTime: "15:30"}}/>
        </DialogContent>
        <DialogActions>
          {!this.props.new &&
            <Button onClick={this.props.handleClose} color="primary">
              Sprechstunde loeschen
            </Button>
          }
          <Button onClick={this.props.handleClose} color="primary">
            Abbrechen
          </Button>
          {this.props.new ?
            <Button onClick={this.props.handleClose} color="primary">
              Sprechstunde erstellen
            </Button> :
            <Button onClick={this.props.handleClose} color="primary">
              Speichern
            </Button>
          }
        </DialogActions>
      </Dialog>
    )
  }
}

CreateConsultationDialog.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(CreateConsultationDialog));
