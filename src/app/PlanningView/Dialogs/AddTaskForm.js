// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import FormLabel from '@material-ui/core/FormLabel';

// Importing components


const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    marginLeft: 0,
    width: 'calc(28% - 8px)',
    minWidth: 200
  },
  textField: {
    margin: theme.spacing.unit,
    width: 'calc(28% - 16px)',
  },
  button: {
    width: 'calc(16% - 8px)',
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: theme.spacing.unit,
    marginRight: 0,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class AddTaskForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };
//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <>
    <FormLabel component="legend">
      Taetigkeiten zu Vorlage hinzufuegen
    </FormLabel>
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel
       ref={ref => {
         this.InputLabelRef = ref;
           }}
       htmlFor={this.props.value}
       >
         Taetigkeit
       </InputLabel>
       <Select
         native
         value={this.props.value}
         onChange={this.handleChange(this.props.stateKey)}
         input={
           <OutlinedInput
           name={this.props.name}
           labelWidth={this.state.labelWidth}
           id= {this.props.value}
           />
         }
       >
         <option key="4" value="4">VU</option>
         <option key="3" value="3">Tropfer</option>
         <option key="2" value="2">Schreiber</option>
         <option key="1" value="1">Mitlaeufer</option>
       </Select>
    </FormControl>
    <TextField
          id="outlined-number"
          label="Anfang"
          value={this.state.age}
          onChange={this.handleChange('age')}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          variant="outlined"
        />
        <TextField
          id="outlined-number"
          label="Ende"
          value={this.state.age}
          onChange={this.handleChange('age')}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
          variant="outlined"
        />
    <Button variant="outlined" color="primary" className={classes.button}>
      Hinzufuegen
    </Button>
    </>
  );
}}

AddTaskForm.propTypes = {classes: PropTypes.object.isRequired};
AddTaskForm = connect(mapStateToProps, mapDispatchToProps)(AddTaskForm);
export default (withStyles(styles)(AddTaskForm));
