// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';

// Importing components


const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit,
  },
});


class DeleteTemplateDialogForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <form className={classes.root}>
        <FormControl fullWidth variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Wochenvorlage
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="2" value="2">Gerade KW</option>
            <option key="1" value="1">Ungerade KW</option>
          </Select>
        </FormControl>
      </form>
    );
  }}

DeleteTemplateDialogForm.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(DeleteTemplateDialogForm));
