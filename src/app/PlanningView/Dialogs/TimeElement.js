// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { TimePicker } from 'material-ui-pickers';

// Importing components

const styles = theme => ({

});

class TimeElement extends Component {
  constructor(props) {
      super(props);
      this.state = {
        selectedTime: new Date(),
      };
    }

    handleChange = time => {
        this.setState({ selectedTime: time });
      };


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <TimePicker
      keyboard
      clearable
      ampm={false}
      cancelLabel
      invalidDateMessage="Ungültige Eingabe"
      mask={[/\d/, /\d/, ':', /\d/, /\d/,]}
      minutesStep={15}
      value={this.state.selectedTime}
      onChange={this.handleChange}
    />
  );
}}

TimeElement.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(TimeElement));
