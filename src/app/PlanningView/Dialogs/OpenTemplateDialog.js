// Importing React
import React, { Component, Fragment } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

// Importing components
import OpenTemplateDialogForm from './OpenTemplateDialogForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

class OpenTemplateDialog extends Component {
  constructor(props) {
      super(props);
      this.state = {
        direction: 'row',
        justify: 'space-around',
        alignItems: 'flex-start',
      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes, consultation } = this.props;

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth='lg'
      >
        <DialogTitle id="form-dialog-title">
          Vorlage in aktuelle Woche laden
        </DialogTitle>
        <DialogContent>
            <DialogContentText>
              Bitte wählen Sie die gewünschte Vorlage aus:<br />
              (Achtung bestehende Eintraege werden durch die Vorlage ueberschrieben!)
            </DialogContentText>
            <OpenTemplateDialogForm />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleClose} color="primary">
            Abbrechen
          </Button>
          <Button onClick={this.props.handleClose} color="primary">
            Vorlage laden
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

OpenTemplateDialog.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(OpenTemplateDialog));
