// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';


import Icon from '@material-ui/core/Icon';
import { IconButton } from '@material-ui/core';


// Importing other libraries
import classNames from 'classnames';
import moment from 'moment';
import { DatePicker } from 'material-ui-pickers';
import isValid from 'date-fns/isValid';
import format from 'date-fns/format';
import isSameDay from 'date-fns/isSameDay';
import startOfWeek from 'date-fns/startOfWeek';
import endOfWeek from 'date-fns/endOfWeek';
import isWithinInterval from 'date-fns/isWithinInterval';


function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  datePicker: {
    minWidth: 50,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  button: {
    margin: theme.spacing.unit,
  },
  calendarButton: {
    margin: theme.spacing.unit,
    width: 40,
    height: 40,
    backgroundColor: `${theme.palette.primary.light}`
  },
  dayWrapper: {
    position: 'relative',
  },
  day: {
    width: 36,
    height: 36,
    fontSize: theme.typography.caption.fontSize,
    margin: '0 2px',
    color: 'inherit',
  },
  customDayHighlight: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: '2px',
    right: '2px',
    border: `1px solid ${theme.palette.secondary.main}`,
    borderRadius: '50%',
  },
  nonCurrentMonthDay: {
    color: theme.palette.text.disabled,
  },
  highlightNonCurrentMonthDay: {
    color: '#676767',
  },
  highlight: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  firstHighlight: {
    extend: 'highlight',
    borderTopLeftRadius: '50%',
    borderBottomLeftRadius: '50%',
  },
  endHighlight: {
    extend: 'highlight',
    borderTopRightRadius: '50%',
    borderBottomRightRadius: '50%',
  },

});

class WeekdayTabs extends Component {

  constructor(props) {
      super(props);
      this.state = {
        dateOfToday: new Date(),
      };
    }

// FUCKED UP function
// Necessary because of the ugly nesting of the select inside the Tabs-tag
// --> Changing the select triggers the onChange-method of the Tabs-tag
// --> Distinction between the two cases inside of the onChange-method
// based on the input
  handleChange = (event, value) => {
    if (event instanceof Date) {
      this.props.handleChangeDate(event);
    }
    else if (value){
      let dateShift = value - this.props.currentWeekday;
      let newDate = new Date(this.props.currentDate.getTime()+dateShift*1000*60*60*24);
      this.props.handleChangeDate(newDate);
    }
  };





//---------------------------DATE-PICKER-FUNCTIONS-------------------------//
//-------------------------------------------------------------------------//

  renderWrappedWeekDay = (date, selectedDate, dayInCurrentMonth) => {
      const { classes } = this.props;

      if (date instanceof moment) {
        date = date.toDate();
      }

      const start = startOfWeek(selectedDate, {weekStartsOn: 1});
      const end = endOfWeek(selectedDate, {weekStartsOn: 1});

      const dayIsBetween = isWithinInterval(date, { start, end });
      const isFirstDay = isSameDay(date, start);
      const isLastDay = isSameDay(date, end);

      const wrapperClassName = classNames({
        [classes.highlight]: dayIsBetween,
        [classes.firstHighlight]: isFirstDay,
        [classes.endHighlight]: isLastDay,
      });

      const dayClassName = classNames(classes.day, {
        [classes.nonCurrentMonthDay]: !dayInCurrentMonth,
        [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth && dayIsBetween,
      });

      return (
        <div className={wrapperClassName}>
          <IconButton className={dayClassName}>
            <span> { format(date, 'D')} </span>
          </IconButton>
        </div>
      );
    }

    formatWeekSelectLabel = (date, invalidLabel) => {
    if (date === null) {
      return '';
    }

    if (date instanceof moment) {
      date = date.toDate();
    }

    return date && isValid(date)
      ? `Week of ${format(startOfWeek(date), 'MMM Do')}`
      : invalidLabel;
  }

  renderInput = (props: TextFieldProps): any => {
    return (<IconButton className={this.props.classes.calendarButton}
      onClick={props.onClick} aria-label='Delete'>
      <Icon color='action'>
        date_range
      </Icon>
    </IconButton>)
  }

//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

returnDateString = (timestamp, adder) => {
  timestamp = new Date(new Date(timestamp).getTime()+1000*60*60*24*adder);
  let day = timestamp.getDate();
  let month = timestamp.getMonth() + 1;
  let year = timestamp.getFullYear();
  let dateString = day + '.' + month + '.' + year;
  return (dateString);
}


//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

  returnTemplateOptions = (id) => {
    let name = this.props.templates[id].name;
    return (
        <option key={id} value={id}>{name}</option>
      )
    };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes } = this.props;
    //const templateKeys = Object.keys(this.props.templates);
    return (
      <div className={classes.root}>
        <AppBar position="static">

          <Tabs value={this.props.currentWeekday} onChange={this.handleChange}>

            <DatePicker
            TextFieldComponent={this.renderInput}
            value={this.state.dateOfToday}
            showTodayButton
            renderDay={this.renderWrappedWeekDay}
            labelFunc={this.formatWeekSelectLabel}
            />
            <Tab value={1} label={"Montag " +
            this.returnDateString(this.props.dateOfMonday, 0)} />
            <Tab value={2} label={"Dienstag " +
            this.returnDateString(this.props.dateOfMonday, 1)} />
            <Tab value={3} label={"Mittwoch " +
            this.returnDateString(this.props.dateOfMonday, 2)} />
            <Tab value={4} label={"Donnerstag " +
            this.returnDateString(this.props.dateOfMonday, 3)} />
            <Tab value={5} label={"Freitag "  +
            this.returnDateString(this.props.dateOfMonday, 4)} />
            <Tab value={6} label={"Samstag " +
            this.returnDateString(this.props.dateOfMonday, 5)} />
            <Tab value={7} label={"Sonntag " +
            this.returnDateString(this.props.dateOfMonday, 6)} />

          </Tabs>
        </AppBar>
      </div>
    );
  }
}

WeekdayTabs.propTypes = {classes: PropTypes.object.isRequired};

export default withStyles(styles)(WeekdayTabs);
