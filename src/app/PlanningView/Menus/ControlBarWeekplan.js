// Main Menu for the App
// This gets imported by App.js

// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

// const for Material-UI settings
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  formControl: {
    marginLeft: 25,
    marginRight: 25,
    width: 200
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  button: {
    margin: theme.spacing.unit,
  }
});

class ControlBarWeekplan extends Component {
// Setting state for the Menu
// This is pretty basic stuff from the Material-UI examples, so I will not comment further
// If you need more information regardless, hit me up and I'll explain further
  constructor(props) {
      super(props);
      this.state = {
        };
      }

//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

// Load weekplan that was selected in DropDown-Menu
readWeekplanTemplate = key => event => {
  this.props.handleReadWeekplanTemplate(event.target.value);
};


//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

  returnWeekplanTemplateOptions = (id) => {
    let name = this.props.weekplanTemplates[id].name;
    return (
        <option key={id} value={id}>{name}</option>
      )
    };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//
  render () {
    const { classes } = this.props;
    const templateKeys = Object.keys(this.props.weekplanTemplates);

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
          {/* Weekplan Buttons */}
          <Button variant="contained" className={classes.button}
          onClick={this.props.handleSaveWeekplan} aria-label='Save Weekplan'>
            Plan speichern
          </Button>

          <Button variant="contained" className={classes.button}
          onClick={this.props.handleEmptyDayplan} aria-label='Reset Dayplan'>
            Plan leeren
          </Button>

          {/*<Button variant="outlined" className={classes.button}
          onClick={this.props.handleEmptyWeekplan} aria-label='Save Weekplan'>
            Wochenplan leeren
          </Button>*/}

          <Button variant="contained" className={classes.button}
          onClick={this.props.handleEmptyEmployeesDayplan} aria-label='Reset Services'>
            Sprechstunden leeren
          </Button>






        {/* Template Select */}

        <FormControl className={classes.formControl}>
          <NativeSelect
          value={this.props.currentWeekplanTemplateId ? this.props.currentWeekplanTemplateId : ""}
          name="templates"
          onChange={this.readWeekplanTemplate()}
          >
          <option value=""/>
          {this.props.weekplanTemplates ? templateKeys.map(id => (this.returnWeekplanTemplateOptions(id))) : undefined}
          </NativeSelect>
        </FormControl>


        {/* Template Buttons */}

        <Button variant="contained" className={classes.button}
        onClick={this.props.handleCreateWeekplanTemplateDialog} aria-label='Create Template'>
          Vorlage Erstellen
        </Button>

        <Button variant="contained" className={classes.button}
        onClick={this.props.handleSaveWeekplanTemplate} aria-label='Update Template'>
          Vorlage Speichern
        </Button>

        <Button variant="contained" className={classes.button}
        onClick={this.props.handleDeleteWeekplanTemplate} aria-label='Vorlage Löschen'>
          Vorlage Löschen
        </Button>

        <Button variant="contained" className={classes.button}
        onClick={window.print} aria-label='Tagesplan drucken'>
          Tagesplan drucken
        </Button>

        <Button variant="contained" className={classes.button}
        onClick={() => this.props.testFunction()} aria-label='Tagesplan drucken'>
          BackEnd Test
        </Button>

          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

ControlBarWeekplan.propTypes = {classes: PropTypes.object.isRequired};

export default withStyles(styles)(ControlBarWeekplan);
