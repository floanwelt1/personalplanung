import React, {Component} from 'react';

import { DragSource } from 'react-dnd';
import ItemTypes from '../../../../Constants';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';
import Schedule from '@material-ui/icons/Schedule';
import Clear from '@material-ui/icons/Clear';
import Brightness3 from '@material-ui/icons/Brightness3';
import EventAvailable from '@material-ui/icons/EventAvailable';
import ChatBubbleOutline from '@material-ui/icons/ChatBubbleOutline';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';

const styles = {
  container: {
    width: 'auto',
    height: 'auto',
  },
  card: {
    height: 50,
    width: 150,
    margin: 2,
    padding: 0,
    display: 'inline-block',
    float: 'top',
  },
  cardContent: {
    padding: 0,
    margin: 0,
    fontSize: 12,
  },
  title: {
    padding: 0,
    margin: 2,
  },
};

const mitarbeiterSource = {
  isDragging(props, monitor) {
    // If your component gets unmounted while dragged
    // (like a card in Kanban board dragged between lists)
    // you can implement something like this to keep its
    // appearance dragged:
    return (props.id === monitor.getItem().id)
  },

  beginDrag(props, monitor, component) {
    // Return the data describing the dragged item
		return ({ id: props.id })
  },

  endDrag(props, monitor, component) {

  }

};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
    connectDragPreview: connect.dragPreview()
  }
}



class EmployeeCard extends Component {


//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//



//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

  setBackgroundColor = () => {
    let taskLevel = this.props.employee.qualifications[this.props.selectedTask];
    let backgroundColor;
    if (taskLevel === '1') {
      backgroundColor = '#76FF03';
    } else if (taskLevel === '2') {
      backgroundColor = '#FFFF00';
    } else if (taskLevel === '3') {
      backgroundColor = '#FF3D00'
    }
    return {backgroundColor: backgroundColor}
  }

  returnWeekNumber = () => {
    let date = new Date(this.props.dateOfMonday);

    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    month += 1; //use 1-12
    let a = Math.floor((14-(month))/12);
    let y = year+4800-a;
    let m = (month)+(12*a)-3;
    let jd = day + Math.floor(((153*m)+2)/5) +
                 (365*y) + Math.floor(y/4) - Math.floor(y/100) +
                 Math.floor(y/400) - 32045;      // (gregorian calendar)
    //var jd = (day+1)+Math.Round(((153*m)+2)/5)+(365+y) +
    //                 Math.round(y/4)-32083;    // (julian calendar)

    //now calc weeknumber according to JD
    let d4 = (jd+31741-(jd%7))%146097%36524%1461;
    let L = Math.floor(d4/1460);
    let d1 = ((d4-L)%365)+L;
    let NumberOfWeek = Math.floor(d1/7) + 1;
    return (NumberOfWeek);
}

//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

  getDateString = (timestamp) => {
    timestamp = new Date(timestamp);
    let dateString = timestamp.getDate() + '.' + (timestamp.getMonth() + 1) + '.' + timestamp.getFullYear();
    return (dateString)
  }

  createHolidayString = () => {
    let holidayStart = '';
    let holidayEnd = '';
    if (this.props.employee.holidayStart) {
      holidayStart = this.getDateString(this.props.employee.holidayStart);
    }
    if (this.props.employee.holidayEnd) {
      holidayEnd = this.getDateString(this.props.employee.holidayEnd);
    }
    let holidayString = holidayStart + ' - ' + holidayEnd;
    return(holidayString)
  }

  createPreferenceString = () => {
    let weekNumber = this.returnWeekNumber();
    if (this.props.employee.preference === '_irrelevant') {
      return('Präferenz: Egal')
    } else if (this.props.employee.preference === '_early' && weekNumber%2 === 0) {
      return ('Präferenz: Früh')
    } else if (this.props.employee.preference === '_early' && weekNumber%2 === 1) {
      return ('Präferenz: Spät')
    }else if (this.props.employee.preference === '_late' && weekNumber%2 === 0) {
      return ('Präferenz: Spät')
    } else if (this.props.employee.preference === '_late' && weekNumber%2 === 1) {
      return ('Präferenz: Früh')
    }
  }
//-----------------------------TO_DELETE_AFTER_DEBUGGING--------------------//
//--------------------------------------------------------------------------//



//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;
    const { isDragging, connectDragSource } = this.props;
    const opacity = isDragging ? 0.3 : 1;
    const name = this.props.employee.firstName + ' ' + this.props.employee.lastName;
    const backgroundColor = this.setBackgroundColor();
    return connectDragSource(
      <div style={{opacity}} className={classes.container}>
        <Card className={classes.card} style={backgroundColor}>
          <CardContent className={classes.cardContent} >
            <Typography className={classes.title} variant="body2">
              {name}
            </Typography>
            Praeferenz: egal | Urlaub: 7/20
            <br />
            Stunden: 17/40 h | 137/177 h
          </CardContent>
        </Card>
      </div>,
      { dropEffect: 'copy'}
    );
  }
}

EmployeeCard.propTypes = {classes: PropTypes.object.isRequired,};

export default (DragSource(ItemTypes.MITARBEITER, mitarbeiterSource, collect)(withStyles(styles)(EmployeeCard)))
