// Main Menu for the App
// This gets imported by App.js

// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

import EmployeeCard from './EmployeeCard';


// const for Material-UI settings
const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    position: 'fixed',
    right: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '75%',
    width: 180,
    paddingTop: 15,
    paddingBottom: 15,
    overflow: 'scroll',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class EmployeeList extends Component {
// Setting state for the Menu
// This is pretty basic stuff from the Material-UI examples, so I will not comment further
// If you need more information regardless, hit me up and I'll explain further
  constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        anchorItem: this.props.page,
        };
      }



//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//



//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

filterHoliday = (employeeKeys) => {
  let adder = 0;
  switch (this.props.weekday) {
    case 'mon':
      adder = 0;
      break;
    case 'tue':
      adder = 1;
      break;
    case 'wed':
      adder = 2;
      break;
    case 'thu':
      adder = 3;
      break;
    case 'fri':
      adder = 4;
      break;
    case 'sat':
      adder = 5;
      break;
  }
  let timestamp = new Date(new Date(this.props.dateOfMonday).getTime()+1000*60*60*24*adder);
  let reducedEmployeeKeys = [];
  for (let i=0; i < employeeKeys.length; i++) {
    let holidayStart = new Date(this.props.employees[employeeKeys[i]].holidayStart);
    let holidayEnd = new Date(this.props.employees[employeeKeys[i]].holidayEnd);
    if (holidayStart > timestamp || holidayEnd < timestamp) {
      reducedEmployeeKeys.push(employeeKeys[i])
    }
  }
  return (reducedEmployeeKeys);
}

filterTask = (employeeKeys) => {
  let task3 = [];
  let task2 = [];
  let task1 = [];
  for (let i=0; i < employeeKeys.length; i++) {
    let taskLevel = this.props.employees[employeeKeys[i]].qualifications[this.props.selectedTask];
    if (taskLevel === '3') {
      task3.push(employeeKeys[i])
    } else if (taskLevel === '2') {
      task2.push(employeeKeys[i])
    } else if (taskLevel === '1') {
      task1.push(employeeKeys[i])
    }
  }
  let reducedEmployeeKeys = task1.concat(task2, task3);
  return(reducedEmployeeKeys)
}

//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

createEmployeeList = () => {
  let employeeKeys = Object.keys(this.props.employees);
  employeeKeys = this.filterHoliday(employeeKeys);
  if (this.props.selectedTask) {
    console.log(this.props.selectedTask);
    employeeKeys = this.filterTask(employeeKeys);
  }
  return (employeeKeys)
}

//-----------------------------TO_DELETE_AFTER_DEBUGGING--------------------//
//--------------------------------------------------------------------------//



//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

    const { classes } = this.props;
    const employeeKeys = this.createEmployeeList();
    return (
      <Paper id="employeelist" className={classes.root}>

      <GridList cellHeight='auto' cols={1}>
      {employeeKeys.map((employeeId, index) => (

        <GridListTile key={employeeId} cols={1}>
          <EmployeeCard key={employeeId}
          id={employeeId}
          employee={this.props.employees[employeeId]}
          selectedTask={this.props.selectedTask}
          dateOfMonday={this.props.dateOfMonday}
          />
        </GridListTile>
      ))}

    </GridList>

    </Paper>


    );
  }
}

EmployeeList.propTypes = {classes: PropTypes.object.isRequired};

export default withStyles(styles)(EmployeeList);
