// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

// Importing components
import PlanningTable from './PlanningTable/PlanningTable';
import EmployeeList from './EmployeeList/EmployeeList';

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
  },
});

class PlanningContainer extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <PlanningTable tasks={this.props.tasks}
          consultationClasses={this.props.consultationClasses}
          employees={this.props.employees}
          consultations={this.props.consultations}
          selectedTask={this.props.selectedTask}
          weekday={this.props.weekday}
          dateOfMonday={this.props.dateOfMonday}
        />

        {
          this.props.selectedTask &&
          <EmployeeList
            employees={this.props.employees}
            selectedTask={this.props.selectedTask}
            dateOfMonday={this.props.dateOfMonday}
            weekday={this.props.weekday}
          />
        }
      </div>
    )
  }
}

PlanningContainer.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(PlanningContainer));
