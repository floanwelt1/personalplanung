// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

// Importing components
import ConsultationElement from './ConsultationElement';
import createWeekplan from './TableFunctions';

const styles = theme => ({
  planningTable : {
    flexGrow: 1,
    "overflow-x": "scroll",
    float: 'left',
    width: 'auto'
  },
  timeColumn: {
    width: 40,
    padding: 0,
  },
  button: {
    margin: theme.spacing.unit,
    marginLeft:  theme.spacing.unit * 2,
    marginRight:  theme.spacing.unit * 2,
  },
});

class PlanningTable extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

  createTableEntry(item, index) {
    let duration = item.duration * 4;
    if (item.time) {
      return (
        <TableCell component="th" scope="row"key={item.time}>
          {item.name}
        </TableCell>
      )
    } else {
      return (
        <TableCell key={index} rowSpan={duration.toString()}>
          {this.createTableItem(item)}
        </TableCell>
      )
    }
  }

  createTableItem(item) {
    if (Object.keys(this.props.consultationClasses).length !== 0 && item !== '') {
      return(
        <ConsultationElement
          consultation={item}
          name={this.props.consultationClasses[item.consultation].name}
          employees={this.props.employees}
          tasks={this.props.tasks}
          selectedTask={this.props.selectedTask}
          handleDrop={(consultationId, taskKey, employeeId, position) =>
            this.props.handleDrop(consultationId, taskKey, employeeId, position)}
          handleEditConsultation={(consultationId) => this.props.handleEditConsultation(consultationId)}
          handleDeleteConsultation={(consultationId) =>
            this.props.handleDeleteConsultation(consultationId)}
          handleSelectTask={(taskKey) => this.props.handleSelectTask(taskKey)}
        />
      )
    }
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes } = this.props;
    const weekplanArray = createWeekplan(this.props.consultations);

    return (
      <Table padding='dense' className={classes.planningTable}>
        <TableHead>
          <TableRow>
              <TableCell className={classes.timeColumn}>
                  <Fab size="small" color="secondary" aria-label="Add"
                    className={classes.button}
                    onClick={() => {this.props.handleModal(true)}}
                  >
                    <AddIcon />
                  </Fab>
              </TableCell>
              <TableCell>Anmeldung</TableCell>
              <TableCell>Dr. Potter</TableCell>
              <TableCell>Frau Wiesling</TableCell>
              <TableCell>Dr. Schreiber</TableCell>
              <TableCell>Dr. Bauer</TableCell>
              <TableCell>Frau Meier</TableCell>
              <TableCell>Frau Musterfrau</TableCell>
              <TableCell>Dr. Einhorn</TableCell>
              <TableCell>Nebenher</TableCell>
              <TableCell>Telefonzentrale</TableCell>
              <TableCell>Büro</TableCell>
              <TableCell>Terminpläne</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {weekplanArray.map((items, index) => {return(
            <TableRow key={index}>
              {items.map((item, index) => this.createTableEntry(item, index))}
            </TableRow>
          )})}
        </TableBody>
      </Table>
    )
  }
}

PlanningTable.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(PlanningTable));
