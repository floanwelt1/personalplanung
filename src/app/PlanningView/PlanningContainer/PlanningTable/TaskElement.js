// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import { IconButton } from '@material-ui/core';


// Importing components
import TaskItem from './TaskItem';
import DropArea from './DropArea';

const styles = theme => ({
  root: {

  },
  avatar: {
    margin: 5,
    width: 30,
    height: 30,
    'font-size': '14px'
  },
  listitem: {
    'font-size': '12px',
  },
  deleteButton: {
    float: 'right',
    width: 20,
    height: 20
  },
  deleteIcon: {
    fontSize: 12,
  },
  task: {
    minHeight: 34,
    paddingLeft: 15,
    paddingTop: 0,
    paddingBottom: 0,

  },
  employee: {
    paddingLeft: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
  noMargin: {
    margin: 0,
    padding: 0
  }
});

class TaskElement extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//





//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//



//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

createAvatar = (employeeId) => {
  if (employeeId) {
    let employee = this.props.employees[employeeId];
    let avatar = employee.firstName[0] + employee.lastName[0];
      return (avatar)
  }
  else {
    return ('?')
  }
}

createEmployeeString = (employeeId) => {
  if (employeeId) {
    let employee = this.props.employees[employeeId];
    let employeeString = employee.firstName + ' ' + employee.lastName;
    return(employeeString)
  }
  return(employeeId)
}

//-----------------------------TO_DELETE_AFTER_DEBUGGING--------------------//
//--------------------------------------------------------------------------//



//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes } = this.props;
    let room = this.props.room;
    if (room) {
      room = room.toUpperCase();
    }

    return (
      <List dense={true} className={classes.noMargin}>
        <Divider />
        <ListItem button dense={true} disableGutters className={classes.task}>
          <ListItemText
          primary={this.props.name.toUpperCase()}
          secondary={room}
          className={classes.listitem}
          onClick={() => this.props.handleSelectTask(this.props.taskKey)}
          />
        </ListItem>
        {this.props.employeeIds.map((employeeId, index) => (
          <TaskItem index={index} emplyeeId={employeeId}/>
        ))}
      </List>
    )
  }
}

TaskElement.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(TaskElement));
