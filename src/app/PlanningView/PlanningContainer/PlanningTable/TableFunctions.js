export default function createWeekplan(consultations={}) {
  let weekplanArray = [
    [{time: '06:00', name: '06:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '06:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '06:30', name: '06:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '06:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '07:00', name: '07:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '07:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '07:30', name: '07:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '07:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '08:00', name: '08:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '08:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '08:30', name: '08:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '08:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '09:00', name: '09:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '09:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '09:30', name: '09:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '09:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '10:00', name: '10:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '10:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '10:30', name: '10:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '10:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '11:00', name: '11:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '11:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '11:30', name: '11:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '11:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '12:00', name: '12:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '12:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '12:30', name: '12:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '12:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '13:00', name: '13:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '13:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '13:30', name: '13:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '13:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '14:00', name: '14:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '14:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '14:30', name: '14:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '14:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '15:00', name: '15:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '15:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '15:30', name: '15:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '15:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '16:00', name: '16:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '16:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '16:30', name: '16:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '16:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '17:00', name: '17:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '17:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '17:30', name: '17:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '17:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '18:00', name: '18:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '18:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '18:30', name: '18:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '18:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '19:00', name: '19:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '19:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '19:30', name: '19:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '19:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '20:00', name: '20:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '20:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '20:30', name: '20:30'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '20:45', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '21:00', name: '21:00'}, '', '', '', '', '', '', '', '', '', '', '', ''],
    [{time: '21:15', name: ''}, '', '', '', '', '', '', '', '', '', '', '', ''],
  ];

  if (consultations){
    let keysConsultations = Object.keys(consultations);
    let consultationArray = [];
    keysConsultations.forEach(key => consultationArray.push(consultations[key]));

    consultationArray = consultationArray.sort((item1, item2)  => {
      let time1 = new Date(item1.startTime);
      let hours1 = time1.getHours();
      let minutes1 = time1.getMinutes();
      let time2 = new Date(item2.startTime);
      let hours2 = time2.getHours();
      let minutes2 = time2.getMinutes();

      if (hours1 === hours2 && minutes1 === minutes2) {
        return (0);
      } else if (hours1 < hours2) {
        return (-1);
      } else if (hours1 === hours2 && minutes1 < minutes2) {
        return (-1);
      } else {
        return (1);
      }
    });


    let spliceObj = {};
    keysConsultations.forEach(key => {
//      for (let i=0; i<consultationArray.length; i++) {
//        let item = consultationArray[i];
      let item = consultations[key];
      let startTime = new Date(item.startTime);
      let startHours = startTime.getHours();
      let startMinutes = startTime.getMinutes();
      let endTime = new Date(item.endTime);
      let endHours = endTime.getHours();
      let endMinutes = endTime.getMinutes();
      let duration = (endHours - startHours) + ((endMinutes - startMinutes)/60)
      let indexStartTime;
      if (item.fullDay) {
        indexStartTime = 0;
        duration = 15.5;
      } else {
        indexStartTime = findIndexOfStartTime(weekplanArray, item.startTime);
      }
      let indexColumn = 0;
      switch (item.doctor) {
        default:
        indexColumn = -1;
        break;

        case "5b4c02c9fb6fc062bcfcffbd":
        indexColumn = 1;
        break;

        case "5b440575fb6fc043c8ce36fd":
        indexColumn = 2;
        break;

        case "5b4405d3fb6fc043c8ce3710":
        indexColumn = 3;
        break;

        case "5b440610fb6fc043c8ce372e":
        indexColumn = 4;
        break;

        case "5b440629fb6fc043c8ce3731":
        indexColumn = 5;
        break;

        case "5b440647fb6fc043c8ce3732":
        indexColumn = 6;
        break;

        case "5b44066dfb6fc043c8ce3735":
        indexColumn = 7;
        break;

        case "5b440686fb6fc043c8ce3738":
        indexColumn = 8;
        break;

        case "5b4c02f9fb6fc062bcfcffd4":
        indexColumn = 9;
        break;

        case "5b748f65e7179a09abb611ce":
        indexColumn = 10;
        break;

        case "5b748f7ae7179a09abb611d9":
        indexColumn = 11;
        break;

        case "5b748f9ae7179a09abb611fa":
        indexColumn = 12;
        break;
      }
      if (item.consultation === '5b5909d2e7179a264f0a2127') {
        indexColumn = 1;
      } else if (item.consultation === '5b590948e7179a264f0a2085') {
        indexColumn = 9;
      } else if (item.consultation === '5b748f65e7179a09abb611ce') {
        indexColumn = 10;
      } else if (item.consultation === '5b748f7ae7179a09abb611d9') {
        indexColumn = 11;
      } else if (item.consultation === '5b748f9ae7179a09abb611fa') {
        indexColumn = 12;
      }
      let consultationObj = {};
      consultationObj['id'] = item._id;
      consultationObj['consultation'] = item.consultation;
      consultationObj['consultationStart'] = startTime;
      consultationObj['consultationEnd'] = endTime;
      consultationObj['doctor'] = item.doctor;
      consultationObj['tasks'] = item.tasks;
      consultationObj['duration'] = duration;
      weekplanArray[indexStartTime][indexColumn] = consultationObj;
      spliceObj[item._id] = {
        indexStartTime: indexStartTime,
        indexColumn: indexColumn,
        duration: duration
      }
    });
    keysConsultations.forEach(key => {
      let indexStartTime = spliceObj[key].indexStartTime;
      let indexColumn = spliceObj[key].indexColumn;
      let duration = spliceObj[key].duration;
      for (let i=1; i < duration*4; i++) {
        weekplanArray[indexStartTime+i][indexColumn] = 'x';
//          weekplanArray[indexStartTime+i].splice(indexColumn, 1);
      }
    })
    }
    for (let i=0; i<weekplanArray.length; i++) {
      weekplanArray[i] = weekplanArray[i].filter(element => element !== 'x');
    }
  return (weekplanArray)
};

function findIndexOfStartTime(multiArray, item) {
  for (var i = 0; i < multiArray.length; i++) {
    let hours = new Date(item).getHours();
    let minutes = new Date(item).getMinutes();
    if (hours < 10) {
      hours= '0' + hours.toString();
    }
    if (minutes < 10) {
      minutes = '0' + minutes.toString();
    }
    let time = hours + ':' + minutes;
    if (multiArray[i][0].time === time) {
      return (i);
    }
  }
}
