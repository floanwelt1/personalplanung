import React, { Component } from 'react';

import { DropTarget } from 'react-dnd';

import ItemTypes from '../../../../Constants'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';



const styles = {
  root: {
    backgroundColor: '#E3F2FD',
  },

};

const mitarbeiterTarget = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    const taskKey = props.taskKey;
    const position = props.position;
    props.handleDrop(taskKey, item.id, position);
    return (item);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
  };
}

class DropArea extends Component {

  render() {
            const { classes, connectDropTarget } = this.props;

            if (this.props.active) {
              return connectDropTarget(
                <div className={this.props.active ? classes.root : ''}>
                  {this.props.children}
              </div>,
              );
            } else {
              return (
                <div className={this.props.active ? classes.root : ''}>
                  {this.props.children}
              </div>
              );
            }



          }
}

DropArea.propTypes = {classes: PropTypes.object.isRequired};

export default (DropTarget(ItemTypes.MITARBEITER, mitarbeiterTarget, collect)(withStyles(styles)(DropArea)));
