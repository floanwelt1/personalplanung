// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import { IconButton } from '@material-ui/core';


// Importing components
import DropArea from './DropArea';

const styles = theme => ({
  root: {

  },
  avatar: {
    margin: 5,
    width: 30,
    height: 30,
    'font-size': '14px'
  },
  listitem: {
    'font-size': '12px',
  },
  deleteButton: {
    float: 'right',
    width: 20,
    height: 20
  },
  deleteIcon: {
    fontSize: 12,
  },
  task: {
    minHeight: 34,
    paddingLeft: 15,
    paddingTop: 0,
    paddingBottom: 0,

  },
  employee: {
    paddingLeft: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
  noMargin: {
    margin: 0,
    padding: 0
  }
});

class TaskItem extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//





//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//



//----------------------------RENDER-FUNCTIONS-----------------------------//
//-------------------------------------------------------------------------//

createAvatar = (employeeId) => {
  if (employeeId) {
    let employee = this.props.employees[employeeId];
    let avatar = employee.firstName[0] + employee.lastName[0];
      return (avatar)
  }
  else {
    return ('?')
  }
}

createEmployeeString = (employeeId) => {
  if (employeeId) {
    let employee = this.props.employees[employeeId];
    let employeeString = employee.firstName + ' ' + employee.lastName;
    return(employeeString)
  }
  return(employeeId)
}

//-----------------------------TO_DELETE_AFTER_DEBUGGING--------------------//
//--------------------------------------------------------------------------//



//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes } = this.props;
    let room = this.props.room;
    if (room) {
      room = room.toUpperCase();
    }

    return (
      <DropArea
        taskKey={this.props.taskKey}
        position={this.props.index}
        handleDrop={(taskKey, employeeId, position) => this.props.handleDrop(taskKey, employeeId, position)}
        active={(this.props.taskKey === this.props.selectedTask) && true}
      >
        <ListItem  alignItems="flex-start" dense className={classes.employee}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}>
              {this.createAvatar(this.props.employeeId)}
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={this.createEmployeeString(this.props.employeeId)}
            secondary={'08:00 - 14:00<br />(11:00 - 11:45)'} />
          <ListItemSecondaryAction>
            <IconButton className={this.props.classes.deleteButton}
              onClick={() => this.props.handleDrop(this.props.taskKey, '', this.props.index)}
              aria-label='Delete'
            >
              <Icon color='action' style={{ fontSize: 16 }}>
                clear
              </Icon>
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </DropArea>
    )
  }
}

TaskItem.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(TaskItem));
