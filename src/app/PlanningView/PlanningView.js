// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

// Importing components
import ControlBarWeekplan from './Menus/ControlBarWeekplan';
import WeekdayTabs from './Menus/WeekdayTabs';
import PlanningContainer from './PlanningContainer/PlanningContainer';
import CreateConsultationDialog from './Dialogs/CreateConsultationDialog';
import CreateTemplateDialog from './Dialogs/CreateTemplateDialog';
import OpenTemplateDialog from './Dialogs/OpenTemplateDialog';
import SaveTemplateDialog from './Dialogs/SaveTemplateDialog';
import DeleteTemplateDialog from './Dialogs/DeleteTemplateDialog';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  consultations: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingLeft: 0,
  },
  button: {
    margin: theme.spacing.unit,
  },
  iconbutton: {
    float: 'right',
  },
  formControl: {
  margin: theme.spacing.unit,
  minWidth: 200,
  },
  paper: {
    position: 'absolute',
    width: 800,
    left: 300,
    top: 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
});

class PlanningView extends Component {
  constructor(props) {
      super(props);
      this.endpoint = 'http://localhost:8000/';
      this.state = {
        consultationModalState: true,
        consultationModal: {
          _id: null,
          consultationClassId: '',
          renderConsultationForm: false,
          doctor: null,
          startTime: null,
          endTime: null,
          fullDay: false,
          tasks: {},
          rooms: {}
        },
        weekplanTemplateDialogState: false,
        weekplanTemplateName: null,
        selectedTask: null,
      };
    }

    testFunction = () => {
      let weekplan = {
        dateOfMonday: new Date(),
        1: {},
        2: {},
        3: {},
        4: {},
        5: {},
        6: {},
        7: {}
      }
      this.handlePost('test', weekplan);
    }

    ////////////////////////////////////////////////////////////////////////////
    // POST REQUEST TO BACK END --> CREATE DB-ENTRY
    ////////////////////////////////////////////////////////////////////////////
    // 1. Create XMLHttpRequest ObjectID
    // 2. Define endpoint from base-url and collection-Name
    // 3. Convert JSON to String
    // 4. Define request opening
    // 5. Define Request Headers
    // 6. Define what happens, when state changes
    // --> When everything is sent and fine take return Object
    //    (_id of inserted document)
    //    and add it to the created object. Then invoke state update with newly
    //    created object (including the returned id from MongoDB)
    // 7. Send request to backend
      handlePost = (collection, container) => {
        console.log(container);
        delete container._id;
        let request = new XMLHttpRequest();
        let postString = JSON.stringify(container);
        request.open('POST', this.endpoint + collection, true);
        request.setRequestHeader('Content-Type', 'application/json');
        request.onreadystatechange = () => {
          if (request.readyState === 4 && request.status === 200) {
            console.log(request.responseText);
          }
        }
        request.send(postString);
      }

//-------------------------------WEEKDAYTAB-FUNCTIONS------------------------//
//---------------------------------------------------------------------------//

  changeDate (value) {
    this.props.handleChangeDate(value);
    this.markTaskForDrop();
  }

//-----------------------CONSULTATION-MODAL-FUNCTIONS----------------------//
//-------------------------------------------------------------------------//

  // Sets state for consultationModalState to true --> Modal opens
  openConsultationModal = () => {
    this.setState({consultationModalState: true});
  }

  // Sets state for consultationModalState to false --> Modal closes
  // Initialize empty consultationModal container and reset state for it
  closeConsultationModal = () => {
    this.setState({ consultationModalState: false });
    let consultationModal = {
      _id: null,
      consultationClassId: null,
      renderConsultationForm: false,
      doctor: null,
      startTime: null,
      endTime: null,
      fullDay: false,
      tasks: {},
    }
    this.setState({ consultationModal: consultationModal});
    };

  //
  updateConsultationModal = (key, value, specifier) => {
    let stateObj = {...this.state.consultationModal};
    if (key === 'consultationClassId') {
      stateObj[key] = value;
      stateObj['renderConsultationForm'] = true;
      let consultation = this.props.consultationTemplates[value];
      let tasks = consultation.tasks;
      let taskObj = {};
      tasks.forEach(task => {
        let tempObj = {};
        tempObj['slots'] = 0;
        tempObj['employees'] = [];
        tempObj['room'] = null;
        taskObj[task] = tempObj;
      });
      stateObj['fullDay'] = false;
      if (value === '5b5909d2e7179a264f0a2127') {
        // ANMELDUNG
        stateObj['doctor'] = '5b4c02c9fb6fc062bcfcffbd';
      } else if (value === '5b590948e7179a264f0a2085') {
        // NEBENHER
        stateObj['doctor'] = '5b4c02f9fb6fc062bcfcffd4';
        stateObj['fullDay'] = true;
      } else if (value === '5b748f65e7179a09abb611ce') {
        // Telefonzentrale
        stateObj['doctor'] = '5b748f65e7179a09abb611ce';
      } else if (value === '5b748f7ae7179a09abb611d9') {
        // Büro
        stateObj['doctor'] = '5b748f7ae7179a09abb611d9';
      } else if (value === '5b748f9ae7179a09abb611fa') {
        // Terminpläne
        stateObj['doctor'] = '5b748f9ae7179a09abb611fa';
      }
      stateObj['tasks'] = taskObj;
    } else if (specifier === 'employees') {
      //console.log(specifier);
      let tempArray = [];
      for (let i=0; i<value; i++) {
        tempArray.push("");
      }
      stateObj.tasks[key].employees = tempArray;
      stateObj.tasks[key].slots = value;
    } else if (specifier === 'room') {
      stateObj.tasks[key].room = value;
    } else {
      stateObj[key] = value;

    }
    console.log(stateObj);
    this.setState({consultationModal: stateObj});
  }

  // Update change in the consultation Modal
  // --> Triggered on create/save-operation of the modal
  //
  // Check if mandatory field are filled in
  // Check if time is valid
  // Check for collision of consultation with exisiting consultations
  //
  // Create copy of currently existing consultations
  // Create id for new consultation from current timestamp
  //    --> Check if consultation has already an id
  //        (Old consultation is being edited)
  //        If an id already exists, update id to exisiting id
  //
  // Create empty Object for consultation
  // Fill object with the id and the data temporarily stored in state
  //
  // Add/update consultation Object to the copy of the exisiting consultations
  // Call update handle for consultations from Container.js and pass the updated
  // list of all consultations together with the weekday.
  //
  //
  // !!!!!!!! --> Lift state of weekday to avoid having to pass it up.
  //
  //
  // If consultationModalState is true (Modal is visible) call close function
  // for modal --> Modal becomes invisible again.
  handleUpdateConsultations = () => {
    if (!this.checkMandatoryFields()) {
      return (null)
    }
    if (!this.checkValidTime()) {
      return (null)
    }
    if (!this.checkCollision()) {
      return (null)
    }
    let currentConsultations = {...this.props.currentWeekplan[this.props.currentWeekday]};
    let _id = '_' + Date.now();
    if (this.state.consultationModal._id !== null){
      _id = this.state.consultationModal._id;
    }
    let newConsultation = {};
    newConsultation['_id'] = _id;
    newConsultation['consultation'] = this.state.consultationModal.consultationClassId;
    newConsultation['dayOfConsultation'] = this.props.currentWeekday;
    newConsultation['startTime'] = this.state.consultationModal.startTime;
    newConsultation['endTime'] = this.state.consultationModal.endTime;
    newConsultation['fullDay'] = this.state.consultationModal.fullDay;
    newConsultation['doctor'] = this.state.consultationModal.doctor;
    newConsultation['tasks'] = this.state.consultationModal.tasks;
    currentConsultations[_id]= newConsultation;
    this.props.handleUpdateConsultations(currentConsultations);
    if (this.state.consultationModalState) {
      this.closeConsultationModal();
    }
  }

  // Set up consultation modal to edit existing consultation
  //
  // Take consultation from consultation list and create temporary object
  // Set state from object to make it available for editing
  //
  //
  // WHY IS IT NECESSARY TO CREATE TEMPORARY OBJECT INSTEAD OF STORING DIRECTLY???
  //
  //
  // Open consultationModal
  editExistingConsultation = (consultationId) => {
    let consultation = this.props.currentWeekplan[this.props.currentWeekday][consultationId];
    let consultationModal = {
      _id: consultationId,
      consultationClassId: consultation.consultation,
      renderConsultationForm: true,
      doctor: consultation.doctor,
      startTime: consultation.startTime,
      endTime: consultation.endTime,
      fullDay: consultation.fullDay,
      tasks: consultation.tasks,
    }
    this.setState({consultationModal: consultationModal});
    this.openConsultationModal();
  }

//----------------------CREATE-TEMPLATE-DIALOG-FUNCTIONS-------------------//
//-------------------------------------------------------------------------//

  // Opens/closes dialog to create new weekplan and set name for it
  // If boolean is true --> Open Dialog by setting state boolean to true
  // If boolean is false --> Close Dialog by setting state boolean to false
  // and reset weekplanTemplateName empty
  createWeekplanTemplateDialog = (boolean) => {
    this.setState({ weekplanTemplateDialogState: boolean });
    if (!boolean) {
      this.setState({weekplanTemplateName: null});
    }
  };

  // Update the state variable of weekplanTemplateName on change of input
  // Is called on change of input field (typing)
  // Monitors the input through event.target.value and sets the state accordingly
  editWeekplanTemplateName = key => event => {
    this.setState({weekplanTemplateName: event.target.value});
  }


//----------------------------DRAG-&-DROP-FUNCTIONS------------------------//
//-------------------------------------------------------------------------//

  // Handles the drop of a drag & drop object
  //
  handleDrop(consultationId, taskKey, employeeId, position) {
    let weekplanCopy = {...this.props.currentWeekplan};
    weekplanCopy[this.props.currentWeekday][consultationId].tasks[taskKey].employees[position] = employeeId;
    this.props.handleDrop(weekplanCopy);
  }

  // Marks/Unmarks the selected task for drag & drop functionality
  //
  // Clicked taskKey is passed in
  // If it has been selected before set state for selectedTask null
  // else set state for selectedTask to taskKey
  markTaskForDrop = (taskKey) => {
    if (this.state.selectedTask === taskKey) {
      this.setState({selectedTask: null});
    } else {
      this.setState({selectedTask: taskKey});
    }
  }


//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//



//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

  // Adjust timestamp to date, month and year of newDate
  changeDate = (timestamp, newDate) => {
    newDate = new Date(newDate);
    let currentDate = newDate.getDate();
    let currentMonth = newDate.getMonth();
    let currentFullYear = newDate.getFullYear();
    timestamp = new Date(timestamp);
    timestamp.setDate(currentDate);
    timestamp.setMonth(currentMonth);
    timestamp.setFullYear(currentFullYear);
    return (timestamp);
  }

  // Check if all mandatory fields are filled in
  checkMandatoryFields = () => {
    let returnBool = true;
    let message = "Folgende Pflichtfelder sind nicht ausgefüllt:";
    if (this.state.consultationModal.consultationClassId === null) {
      message = message + ' Sprechstundentyp,';
      returnBool = false;
    }
    if (this.state.consultationModal.doctor === null) {
      message = message + ' Arzt,';
      returnBool = false;
    }
    if (!this.state.consultationModal.startTime && !this.state.consultationModal.fullDay) {
      message = message + ' Startzeit,';
      returnBool = false;
    }
    if (!this.state.consultationModal.endTime && !this.state.consultationModal.fullDay) {
      message = message + ' Endzeit,';
      returnBool = false;
    }
    if (!returnBool) {
      message = message.replace(/.$/,".");
      this.props.handleSnackbarOpen(message);
    }
    return (returnBool);
  }

  // Check if consultation time is valid
  checkValidTime = () => {
    let currentDate = this.props.currentDate;
    let startTime = this.changeDate(this.state.consultationModal.startTime, currentDate);
    let endTime = this.changeDate(this.state.consultationModal.endTime, currentDate);
    let startTimeMinutes = startTime.getMinutes();
    let endTimeMinutes = startTime.getMinutes();
    if ((startTimeMinutes === 0 || startTimeMinutes === 15 ||
      startTimeMinutes === 30 || startTimeMinutes === 45) &&
      (endTimeMinutes === 0 || endTimeMinutes === 15 ||
        endTimeMinutes === 30 || endTimeMinutes === 45) &&
      startTime.getTime() <= endTime.getTime() ) {
          return (true);
        } else {
          this.props.handleSnackbarOpen('Die aktuell gewählte Endzeit liegt vor oder gleich der Startzeit');
          return (false);
        }
  }

  // Check if consultation collides with exisiting consultations
  checkCollision = () => {
    let currentDate = this.props.currentDate;
    let currentStartTime = this.changeDate(this.state.consultationModal.startTime, currentDate);
    let currentEndTime = this.changeDate(this.state.consultationModal.endTime, currentDate);
    let consultations = Object.keys(this.props.currentWeekplan[this.props.currentWeekday])
    for (let i=0; i<consultations.length; i++) {
      let consultation = this.props.currentWeekplan[this.props.currentWeekday][consultations[i]];
      let consultationStartTime = this.changeDate(consultation.startTime, currentDate);
      let consultationEndTime = this.changeDate(consultation.endTime, currentDate);
      if ((consultation._id !== this.state.consultationModal._id &&
        consultation.doctor === this.state.consultationModal.doctor &&
        ((currentStartTime >= consultationStartTime && currentStartTime < consultationEndTime) ||
        (currentEndTime > consultationStartTime && currentEndTime <= consultationEndTime) ||
        (currentStartTime <= consultationStartTime && currentEndTime >= consultationEndTime))) ||
        (consultation.consultation === '5b590948e7179a264f0a2085' &&
          this.state.consultationModal.consultationClassId === '5b590948e7179a264f0a2085')) {
        this.props.handleSnackbarOpen('Sprechstunde kollidiert mit bestehender Sprechstunde');
        return(false);
      }
    }
    return (true);
  }

  // Extract the names of the weekplan templates
  getWeekplanTemplateNames () {
    const keys = Object.keys(this.props.weekplanTemplates);
    let weekplanTemplateNames = {};
    keys.map(Id =>
      (weekplanTemplateNames[Id] = this.props.weekplanTemplates[Id].name));
    return (weekplanTemplateNames);
  }


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <div className={classes.root}>
        <ControlBarWeekplan
        currentWeekplanTemplateId={this.props.currentWeekplanTemplateId}
        weekplanTemplates={this.props.weekplanTemplates}
        handleSaveWeekplan={() => (this.props.handleSaveWeekplan())}
        handleEmptyWeekplan={() => (this.props.handleEmptyWeekplan())}
        handleEmptyDayplan={() => (this.props.handleEmptyDayplan())}
        handleEmptyEmployeesDayplan={() => (this.props.handleEmptyEmployeesDayplan())}
        handleCreateWeekplanTemplateDialog={() => (this.createWeekplanTemplateDialog(true))}
        handleReadWeekplanTemplate={(templateId) => {this.props.handleReadWeekplanTemplate(templateId)}}
        handleSaveWeekplanTemplate={() => (this.props.handleSaveWeekplanTemplate())}
        handleDeleteWeekplanTemplate={() => (this.props.handleDeleteWeekplanTemplate())}
        testFunction={() => this.testFunction()}
        />

        <WeekdayTabs
        currentDate={this.props.currentDate}
        currentWeekday={this.props.currentWeekday}
        dateOfMonday={this.props.currentWeekplan ? this.props.currentWeekplan.dateOfMonday : new Date()}
        handleChangeDate={(newDate) => {this.changeDate(newDate)}}
        />

        {/* Planning Interface */}
        <PlanningContainer
        tasks={this.props.tasks}
        consultationClasses={this.props.consultationTemplates}
        employees={this.props.employees}
        consultations={this.props.currentWeekplan && this.props.currentWeekplan[this.props.currentWeekday]}
        selectedTask={this.state.selectedTask}
        weekday={this.props.currentWeekday}
        dateOfMonday={this.props.currentWeekplan ? this.props.currentWeekplan.dateOfMonday : new Date()}
        handleModal={() => this.openConsultationModal()}
        handleDrop={(consultationId, taskKey, employeeId, position) => this.handleDrop(consultationId, taskKey, employeeId, position)}
        handleEditConsultation={(consultationId) => this.editExistingConsultation(consultationId)}
        handleDeleteConsultation={(consultationId) => this.props.handleDeleteConsultation(this.props.currentWeekday, consultationId)}
        handleSelectTask={(taskKey) => this.markTaskForDrop(taskKey)}
        />

        <CreateConsultationDialog open={false} new={false} />
        <CreateTemplateDialog open={false} new={false} />
        <OpenTemplateDialog open={false} new={false} />
        <SaveTemplateDialog open={false} new={false} />
        <DeleteTemplateDialog open={false} new={false} />
    </div>
  );
}}

PlanningView.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(PlanningView));
