// Main container for the App
// This file gets imported by index.js

// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';



//Importing Material-UI
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import red from '@material-ui/core/colors/red';
import yellow from '@material-ui/core/colors/yellow';

// DatePicker
import { MuiPickersUtilsProvider}  from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
// import DateFnsUtils from '@date-io/date-fns';

// Importing Components
import MainMenu from './MainMenu';
import PlanningView from './PlanningView/PlanningView';
import RessourcesView from './RessourcesView/RessourcesView';
import CalenderView from './CalenderView/CalenderView';
import ExportView from './ExportView/ExportView';

// Importingindividual styling CSS
import './App.css';


// For the Material-UI Theme, to style if wanted later
  const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: yellow,
    error: red,
  },
  typography: {
    useNextVariants: true,
    fontSize: 12
  },
});


function MainView(props) {
  switch(props.mainView) {
    case 'planningView':
    return (<PlanningView tasks={props.falseProps.tasks}
    // REMOVE ALL THAT BS FROM HERE ONCE PLANNINGVIEW IS REFACTORED!!!!!
    consultationTemplates={props.falseProps.consultationTemplates}
    employees={props.falseProps.employees}
    doctors={props.falseProps.doctors}
    currentDate={props.falseProps.currentDate}
    currentWeekday={props.falseProps.currentWeekday}
    currentWeekplan={props.falseProps.currentWeekplan}
    currentWeekplanTemplateId={props.falseProps.currentWeekplanTemplateId}
    weekplanTemplates={props.falseProps.weekplanTemplates}/>)
    // STOP REMOVING HERE
    break;
    case 'ressourcesView':
    return (<RessourcesView />)
    break;
    case 'calenderView':
    return (<CalenderView />)
    break;
    case 'exportView':
    return (<ExportView />)
    break;
    default:
    break;
  }
}

class App extends Component {



  render() {
    const falseState = {
      currentDate: new Date(),
      currentWeekday: null,
      tasks: {},
      consultationTemplates: {},
      employees: {},
      doctors: {},
      weekplanTemplates: {},
      weekplans: {},
      currentWeekplan: {
        dateOfMonday: new Date(),
        1: {},
        2: {},
        3: {},
        4: {},
        5: {},
        6: {},
        7: {}
      },
      currentWeekplanId: null,
      currentWeekplanTemplateId: '',
      currentConsultationTemplateId: '',
      currentEmployee: '',
      currentDoctor: '',
      snackbarState: false,
    };
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>

        <MuiThemeProvider theme={theme}>
          <div className="App">
          <CssBaseline />

          {/* UI ELEMENTS */}
          <MainMenu />
          <MainView falseProps={falseState} mainView={this.props.main.mainView} />

          </div>
        </MuiThemeProvider>

      </MuiPickersUtilsProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    main: state.main
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
