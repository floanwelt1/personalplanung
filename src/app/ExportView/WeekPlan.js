// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

// Importing components
import PreferenceCell from './PreferenceCell';
import TaskView from './TaskView';

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  cell: {
    padding: theme.spacing.unit,
    border: '1px solid grey',
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class WeekPlan extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <Table padding='dense' id="#print" className={classes.root}>
        <TableHead>
          <TableRow>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Montag</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Dienstag</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Mittwoch</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Donnerstag</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Freitag</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Samstag</Typography></TableCell>
            <TableCell className={classes.cell} align="center"><Typography variant="subtitle1">Sonntag</Typography></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Frueh' absence={"Schule"} /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Frueh' /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Spaet' /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Egal' absence={"Urlaub"} /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Nein' /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Frueh' /></TableCell>
            <TableCell className={classes.cell} align="center"><PreferenceCell preference='Nein' /></TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
            <TableCell className={classes.cell} align="center"><TaskView consultation={"GKV"} consultationTime={"08:00 - 15:00"} doctor_category={"Dr. Potter"} /></TableCell>
          </TableRow>
        </TableBody>
      </Table>
    );
  }
}

WeekPlan.propTypes = {classes: PropTypes.object.isRequired};
WeekPlan = connect(mapStateToProps, mapDispatchToProps)(WeekPlan);
export default (withStyles(styles)(WeekPlan));
