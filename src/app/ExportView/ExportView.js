// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import ListMenu from './ListMenu';
import PrintForm from './PrintForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ExportView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <Grid
      container
      className={classes.root}
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      >
        <Grid item xs={12}>
          <h4>Dienstplanexport</h4>
        </Grid>
        <Grid item xs={2}>
          <ListMenu />
        </Grid>
        <Grid item xs={10}>
          <PrintForm />
        </Grid>
      </Grid>
    );
  }}

ExportView.propTypes = {classes: PropTypes.object.isRequired};
ExportView = connect(mapStateToProps, mapDispatchToProps)(ExportView);
export default (withStyles(styles)(ExportView));
