// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';

// Importing components

// const for Material-UI
const styles = theme => ({

});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class PreferenceCell extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    return (
      <>
        <Typography variant='subtitle1'>{this.props.preference}</Typography>
        <Typography variant='subtitle1'>{this.props.absence ? this.props.absence : '-'}</Typography>
      </>
    );
  }
}

PreferenceCell.propTypes = {classes: PropTypes.object.isRequired};
PreferenceCell = connect(mapStateToProps, mapDispatchToProps)(PreferenceCell);
export default (withStyles(styles)(PreferenceCell));
