// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

// Importing components
import WeekPlan from './WeekPlan';

const styles = theme => ({
  root: {
    margin: theme.spacing.unit * 2,
    padding: theme.spacing.unit * 5,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class PrintForm extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <Paper className={classes.root}>
      <Typography variant="h5">Wochenplan - Mitarbeiter1</Typography>
      <Typography variant="subtitle1" gutterBottom>KW46 / 2019 - 12.11.2019-18.11.2019</Typography>
      <br />
      <WeekPlan />
      <br />
      <Typography variant="body1">Kommentar: Achtung, am Mittwoch bitte bis 12 Uhr freihalten! Bitte ausserdem Rueckmeldung geben, ob naechste Woche am Donnerstag Schichten getauscht werden koennen.</Typography>
    </Paper>
  );
}}

PrintForm.propTypes = {classes: PropTypes.object.isRequired};
PrintForm = connect(mapStateToProps, mapDispatchToProps)(PrintForm);
export default (withStyles(styles)(PrintForm));
