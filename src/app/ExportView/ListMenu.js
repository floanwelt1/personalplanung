// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { DatePicker } from 'material-ui-pickers';

// Importing components

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit,
  },
  employeeSelect: {
    marginTop: theme.spacing.unit,
  },
  datepicker: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
  button: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ListMenu extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
  }

  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
    });
  }


  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <form noValidate autoComplete="off" className={classes.root}>
      <FormControl className={classes.employeeSelect} variant="outlined" fullWidth margin='normal'>
        <InputLabel
          ref={ref => {
            this.InputLabelRef = ref;
          }}
          htmlFor={this.props.value}
        >
          Mitarbeiter
        </InputLabel>
        <Select
          native
          value={this.props.value}
          onChange={this.handleChange(this.props.stateKey)}
          input={
            <OutlinedInput
              name={this.props.name}
              labelWidth={this.state.labelWidth}
              id={this.props.value}
            />
            }
          >
          <option key="1" value="1">Mitarbeiter1</option>
          <option key="2" value="2">Mitarbeiter2</option>
        </Select>
      </FormControl>
      <DatePicker
        className={classes.datepicker}
        keyboard
        mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
        label="Woche"
        format="dd/MM/YYYY"
        value={new Date()}
        onChange={() => this.placeholderFunction()}
        variant="outlined"
      />
      <TextField
       fullWidth
       multiline
       rows="6"
       id="taskName"
       label='Kommentar'
       value={this.props.taskName}
       margin="normal"
       variant="outlined"
       />
       <Button variant="outlined" fullWidth color="primary" className={classes.button}>
         Vorschau
       </Button>
       <Button variant="outlined" fullWidth color="primary" className={classes.button}>
         Mitarbeiterplan exportieren
       </Button>
       <Button variant="outlined" fullWidth color="primary" className={classes.button}>
         Alle Plaene exportieren
       </Button>
     </form>
  );
}}

ListMenu.propTypes = {classes: PropTypes.object.isRequired};
ListMenu = connect(mapStateToProps, mapDispatchToProps)(ListMenu);
export default (withStyles(styles)(ListMenu));
