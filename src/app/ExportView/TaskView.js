// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';

// Importing components
import ConsultationElement from './ConsultationElement';

// const for Material-UI
const styles = theme => ({

});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class TaskView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    return (
      <List component='nav'>
        <ConsultationElement
          consultation={this.props.consultation}
          doctor_category={this.props.doctor_category}
          consultationTime={this.props.consultationTime}
          task={"Mitschreiber"}
          startTime={"08:30"}
          endTime={"15:30"}
          startPause={"11:30"}
          endPause={"12:30"}
          />
        <ConsultationElement
          consultation={"IVOM"}
          doctor_category={"Dr. Hettenbach"}
          consultationTime={"16:00 - 21:00"}
          task={"Setzer"}
          startTime={"16:00"}
          endTime={"20:30"}
          />
        <ConsultationElement
          consultation={"Nebenher"}
          doctor_category={"Nebenher"}
          consultationTime={"Ganztaegig"}
          task={"Op-Richten"}
          />
      </List>
    );
  }
}

TaskView.propTypes = {classes: PropTypes.object.isRequired};
TaskView = connect(mapStateToProps, mapDispatchToProps)(TaskView);
export default (withStyles(styles)(TaskView));
