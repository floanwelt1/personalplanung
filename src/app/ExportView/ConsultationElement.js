// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

// Importing components

// const for Material-UI
const styles = theme => ({
    nested: {
      paddingTop: 0,
      paddingBottom: 0,
    },
    listItemText: {
      marginLeft: theme.spacing.unit * 2,
    },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class ConsultationElement extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;
    
    return (
      <>
        <ListItem alignItems="flex-start">
          <ListItemText primary={this.props.consultation} />
        </ListItem>
        <List component="div" disablePadding>
          <ListItem className={classes.nested}>
            <ListItemText className={classes.listItemText} primary={this.props.doctor_category} secondary={this.props.consultationTime} />
          </ListItem>
          <ListItem className={classes.nested}>
            <ListItemText className={classes.listItemText} primary={this.props.task} secondary={this.props.startTime && this.props.startTime + ' - ' + this.props.endTime} />
          </ListItem>
          <ListItem className={classes.nested}>
            <ListItemText className={classes.listItemText} primary='Pause' secondary={this.props.startPause ? this.props.startPause + ' - ' + this.props.endPause : '-'} />
          </ListItem>
        </List>
      </>
    );
  }
}
ConsultationElement.propTypes = {classes: PropTypes.object.isRequired};
ConsultationElement = connect(mapStateToProps, mapDispatchToProps)(ConsultationElement);
export default (withStyles(styles)(ConsultationElement));
