// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

// Importing components
import AbsenceCalenderView from './AbsenceCalenderView/AbsenceCalenderView';
import GroupRulesView from './GroupRulesView/GroupRulesView';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

function RessourcesContainer(props) {
  switch(props.view) {
    case 'absenceCalender':
    return (<AbsenceCalenderView />)
    case 'groupRules':
    return(<GroupRulesView />)
    default:
    break;
  }
}

class CalenderView extends Component {
  constructor(props) {
      super(props);
      this.state = {
        currentRessource: 'absenceCalender'
      };
    }

  changeRessource = (event, value) => {
    this.setState({currentRessource: value})
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

  return (
    <div>
      <AppBar position="static">
        <Tabs value={this.state.currentRessource} onChange={this.changeRessource} fullWidth>
          <Tab value={'absenceCalender'} label={'Abwesenheitskalender'} />
          <Tab value={'groupRules'} label={'Gruppen & Regeln'} />
        </Tabs>
      </AppBar>
      <RessourcesContainer view={this.state.currentRessource} />
    </div>
  );
}}

CalenderView.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(CalenderView));
