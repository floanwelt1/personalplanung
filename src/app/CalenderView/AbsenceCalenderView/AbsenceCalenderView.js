// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import ListMenu from './ListMenu';
import AbsenceCalenderForm from './AbsenceCalenderForm';
import AbsenceDialog from './AbsenceDialog';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks,
    consultations: state.consultations,
    doctorsCategories: state.doctorsCategories
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class AbsenceCalenderView extends Component {
  constructor(props) {
      super(props);
      this.state = {
        openAbsenceDialog: false,
      };
    }

  handleClickOpen = () => {
    this.setState({ openAbsenceDialog: true });
  };

  handleClose = () => {
    this.setState({ openAbsenceDialog: false });
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return [
      <Grid
      container
      className={classes.root}
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      >
        <Grid item xs={2}>
          <ListMenu
            listInput={this.props.doctorsCategories}
            editAbsenceDialog={() => this.handleClickOpen()}
          />
        </Grid>
        <Grid item xs={10}>
          <AbsenceCalenderForm
            consultations={this.props.consultations}
            createAbsenceDialog={() => this.handleClickOpen()}
            editAbsenceDialog={() => this.handleClickOpen()}
          />
        </Grid>
      </Grid>,
      <AbsenceDialog
        open={this.state.openAbsenceDialog}
        new={false}
        handleClose={() => this.handleClose()}
      />
    ];
  }}

AbsenceCalenderView.propTypes = {classes: PropTypes.object.isRequired};
AbsenceCalenderView = connect(mapStateToProps, mapDispatchToProps)(AbsenceCalenderView);
export default (withStyles(styles)(AbsenceCalenderView));
