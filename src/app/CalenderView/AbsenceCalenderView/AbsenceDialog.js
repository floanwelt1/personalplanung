// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

// Importing components
import DialogForm from './DialogForm';

const styles = theme => ({

});

class AbsenceDialog extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {this.props.new ? "Neue Abwesenheit erstellen" : "Abwesenheit bearbeiten"}
        </DialogTitle>
        <DialogContent>
          <DialogForm />
        </DialogContent>
        <DialogActions>
          {!this.props.new &&
            <Button onClick={this.props.handleClose} color="primary">
              Abwesenheit loeschen
            </Button>
          }
          <Button onClick={this.props.handleClose} color="primary">
            Abbrechen
          </Button>
          {this.props.new ?
            <Button onClick={this.props.handleClose} color="primary">
              Abwesenheit erstellen
            </Button> :
            <Button onClick={this.props.handleClose} color="primary">
              Speichern
            </Button>
          }
        </DialogActions>
      </Dialog>
    );
  }}

AbsenceDialog.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(AbsenceDialog));
