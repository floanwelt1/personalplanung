// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

// Importing components
import YearPicker from './YearPicker';
import AbsenceCalender from './AbsenceCalender';

const styles = theme => ({
  yearPicker: {
    margin: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class AbsenceCalenderForm extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  componentDidMount() {

    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;
  const consultationsKeys = Object.keys(this.props.consultations);

  return (
    <>
    <h4>Wochen-Abwesenheiten</h4>
    <YearPicker class={classes.yearPicker}/>
    <Button
      variant="outlined"
      color="primary"
      className={classes.button}
      onClick={this.props.createAbsenceDialog}
    >
      Neue Abwesenheit erstellen
    </Button>
    <AbsenceCalender />
  </>
  );
}}

AbsenceCalenderForm.propTypes = {classes: PropTypes.object.isRequired};
AbsenceCalenderForm = connect(mapStateToProps, mapDispatchToProps)(AbsenceCalenderForm);
export default (withStyles(styles)(AbsenceCalenderForm));
