// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';

// Importing components

// const for Material-UI
const styles = theme => ({
  nested: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  listItemIcon: {
    marginLeft: theme.spacing.unit,
    marginRight: 0,
  },
  listItemText: {
    padding: 0,
  },
  button: {

  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class AbsenceDayView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }
    
//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <List component='nav' dense>
        <Typography variant='subtitle1'>{this.props.label}</Typography>
        <ListItem>
          <ListItemText primary="Heute im Urlaub:" />
        </ListItem>
        <Divider />
        <List component="div" dense disablePadding>
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter1' secondary="11.01.19 - 18.02.19"/>
          </ListItem>
          <Divider />
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter2' secondary="11.01.19 - 18.02.19" />
          </ListItem>
        </List>
        <Divider />
        <ListItem>
          <ListItemText primary="Heute krank:" />
        </ListItem>
        <Divider />
        <List component="div" dense disablePadding>
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter1' secondary="11.01.19 - 18.02.19"/>
          </ListItem>
          <Divider />
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter2' secondary="11.01.19 - 18.02.19"/>
          </ListItem>
        </List>
        <Divider />
        <ListItem>
          <ListItemText primary="Sonstiges:" />
        </ListItem>
        <Divider />
        <List component="div" dense disablePadding>
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter1' secondary="11.01.19 - 18.02.19" />
          </ListItem>
          <Divider />
          <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary='Mitarbeiter2' secondary="11.01.19 - 18.02.19" />
          </ListItem>
        </List>
      </List>
    );
  }
}

AbsenceDayView.propTypes = {classes: PropTypes.object.isRequired};
AbsenceDayView = connect(mapStateToProps, mapDispatchToProps)(AbsenceDayView);
export default (withStyles(styles)(AbsenceDayView));
