// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

// Importing components
import AbsenceDayView from './AbsenceDayView';

// const for Material-UI
const styles = theme => ({
  calenderBox: {
    margin: theme.spacing.unit,
    maxHeight: 730,
    overflow: 'auto',
  },
  thRowCell: {
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  cell: {
    padding: theme.spacing.unit,
    border: '1px solid grey',
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class AbsenceCalender extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <Paper className={classes.calenderBox}>
        <Table padding='dense'>
          <TableHead>
            <TableRow>
              <TableCell className={classes.thRowCell}>KW</TableCell>
              <TableCell align="center">Montag</TableCell>
              <TableCell align="center">Dienstag</TableCell>
              <TableCell align="center">Mittwoch</TableCell>
              <TableCell align="center">Donnerstag</TableCell>
              <TableCell align="center">Freitag</TableCell>
              <TableCell align="center">Samstag</TableCell>
              <TableCell align="center">Sonntag</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">11</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='11.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='12.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='13.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='14.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='15.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='16.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='17.03.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">12</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='18.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='19.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='20.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='21.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='22.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='23.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='24.03.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">13</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='25.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='26.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='27.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='28.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='29.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='30.03.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='31.03.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">14</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='01.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='02.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='03.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='04.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='05.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='06.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='07.04.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">15</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='08.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='09.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='10.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='11.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='12.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='13.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='14.04.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">16</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='15.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='16.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='17.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='18.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='19.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='20.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='21.04.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">17</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='22.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='23.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='24.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='25.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='26.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='27.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='28.04.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">18</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='29.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='30.04.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='01.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='02.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='03.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='04.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='05.05.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">19</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='06.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='07.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='08.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='09.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='10.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='11.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='12.05.2019' /></TableCell>
              </TableRow>

              <TableRow>
                <TableCell className={classes.thRowCell} component="th" scope="row">20</TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='13.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='14.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='15.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='16.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='17.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='18.05.2019' /></TableCell>
                <TableCell className={classes.cell} align="center"><AbsenceDayView label='19.05.2019' /></TableCell>
              </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

AbsenceCalender.propTypes = {classes: PropTypes.object.isRequired};
AbsenceCalender = connect(mapStateToProps, mapDispatchToProps)(AbsenceCalender);
export default (withStyles(styles)(AbsenceCalender));
