// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';

// Importing components

// const for Material-UI
const styles = theme => ({

});

class YearPicker extends Component {
  constructor(props) {
      super(props);
      this.state = {
        calender: {}
      };
    }

  placeholderFunction = () => {
    console.log("Fix it!");
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  return (
    <>
      <DatePicker
        openToYearSelection
        className={this.props.class}
        keyboard
        mask={[ /\d/, /\d/, /\d/, /\d/]}
        label="Kalenderjahr"
        format="YYYY"
        value={new Date()}
        onChange={() => this.placeholderFunction()}
        variant="outlined"
      />
    </>
  );
}}

YearPicker.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(YearPicker));
