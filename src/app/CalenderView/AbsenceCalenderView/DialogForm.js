// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { DatePicker } from 'material-ui-pickers';

// Importing components

const styles = theme => ({
  datepickerStart: {
    width: 'calc(50% - 8px)',
    marginTop: theme.spacing.unit * 2,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: theme.spacing.unit,
  },
  datepickerEnd: {
    width: 'calc(50% - 8px)',
    marginTop: theme.spacing.unit * 2,
    marginBottom: 0,
    marginLeft: theme.spacing.unit,
    marginRight: 0,

  }
});


class DialogForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <form>
        <FormControl fullWidth variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Mitarbeiter
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="2" value="2">Mitarbeiter1</option>
            <option key="1" value="1">Mitarbeiter2</option>
          </Select>
        </FormControl>
        <FormControl fullWidth variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Abwesenheit
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="1" value="1">Urlaub</option>
            <option key="2" value="2">Krankheit</option>
            <option key="2" value="2">Sonstiges</option>
          </Select>
        </FormControl>
        <TextField
          margin="normal"
          id="comment"
          label="Kommentar"
          fullWidth
          variant="outlined"
        />
        <DatePicker
          className={classes.datepickerStart}
          keyboard
          mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
          label="Startdatum"
          format="dd/MM/YYYY"
          value={new Date()}
          onChange={() => this.placeholderFunction()}
          variant="outlined"
        />
        <DatePicker
          className={classes.datepickerEnd}
          keyboard
          mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
          label="Enddatum"
          format="dd/MM/YYYY"
          value={new Date()}
          onChange={() => this.placeholderFunction()}
          variant="outlined"
        />
      </form>
    );
  }}

DialogForm.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(DialogForm));
