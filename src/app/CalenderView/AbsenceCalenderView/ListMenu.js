// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

// Importing components
import YearPicker from './YearPicker';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  list: {
    width: '100%',
    height: window.innerHeight - (170),
    minHeight: 600,
    maxHeight: 1200,
    position: 'relative',
    overflow: 'auto',
  },
  yearPicker: {
    margin: theme.spacing.unit,
    width: '80%',
  },
  employeeSelect: {
    margin: theme.spacing.unit,
    width: '80%',
  },
  nested: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  listItemIcon: {
    marginLeft: theme.spacing.unit,
    marginRight: 0,
  },
  listItemText: {
    padding: 0,
  },
  button: {

  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ListMenu extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
        leaveOpen: true,
        sickOpen: true,
        miscOpen: true,
      };
  }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleClick = (listKey) => {
    let returnObj = {};
    returnObj[listKey] = !this.state[listKey];
    this.setState(returnObj);

    // this.setState(state => ({ open: !state.open }));
  };

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  const leaveKeys = Object.keys(this.props.listInput);
  const sickKeys = Object.keys(this.props.listInput);
  const miscKeys = Object.keys(this.props.listInput);
  return (
    <div className={classes.root}>
      <h4>Mitarbeiter-Abwesenheiten</h4>
      <List
        className={classes.list}
        component='nav'
      >
        <YearPicker class={classes.yearPicker} />
        <FormControl className={classes.employeeSelect} variant="outlined" margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Mitarbeiter
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
            }
          >
            <option key="2" value="2">Mitarbeiter1</option>
            <option key="1" value="1">Mitarbeiter2</option>
          </Select>
        </FormControl>
        <ListItem button onClick={() => this.handleClick('leaveOpen')}>
          <ListItemText primary="Eingetragene Urlaube" />
          {this.state.leaveOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={this.state.leaveOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Divider />
            <ListItem className={classes.nested}>
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary="12.03.19 - 19.03.19"/>
            </ListItem>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="15.05.19 - 29.05.19"/>
            </ListItem>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="27.09.19 - 20.10.19"/>
            </ListItem>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="23.12.19 - 07.01.20"/>
            </ListItem>
          {/*{leaveKeys.map(key => [
            <Divider />,
            <ListItem
            key={key}
            className={classes.nested}
            >
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary={this.props.listInput[key].name + ' - ' + this.props.listInput[key].name} />
            </ListItem>
          ])}*/}
          </List>
        </Collapse>
        <ListItem button onClick={() => this.handleClick('sickOpen')}>
          <ListItemText primary="Eingetragene Krankheiten" />
          {this.state.sickOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={this.state.sickOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="18.01.19 - 20.01.19"/>
            </ListItem>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="23.01.19 - 26.01.19"/>
            </ListItem>
          {/*         {sickKeys.map(key => [
                    <Divider />,
                    <ListItem
                    key={key}
                    className={classes.nested}
                    >
                      <ListItemIcon className={classes.listItemIcon}>
                        <IconButton className={classes.button} aria-label="Edit" color="primary">
                          <EditIcon fontSize="small"/>
                        </IconButton>
                      </ListItemIcon>
                      <ListItemText className={classes.listItemText} inset primary={this.props.listInput[key].name + ' - ' + this.props.listInput[key].name} />
                    </ListItem>
                  ])} */}
          </List>
        </Collapse>
        <ListItem button onClick={() => this.handleClick('miscOpen')}>
          <ListItemText primary="Sonstige Abwesenheiten" />
          {this.state.miscOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={this.state.miscOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Divider />
            <ListItem className={classes.nested}>
              <ListItemIcon className={classes.listItemIcon}>
                <IconButton className={classes.button} aria-label="Edit" color="primary">
                  <EditIcon fontSize="small"/>
                </IconButton>
              </ListItemIcon>
              <ListItemText className={classes.listItemText} inset primary="11.01.19 - 12.01.19" secondary="Fortbildung" />
            </ListItem>
        {/*{miscKeys.map(key => [
          <Divider />,
          <ListItem
          key={key}
          className={classes.nested}
          >
            <ListItemIcon className={classes.listItemIcon}>
              <IconButton className={classes.button} aria-label="Edit" color="primary">
                <EditIcon fontSize="small"/>
              </IconButton>
            </ListItemIcon>
            <ListItemText className={classes.listItemText} inset primary={this.props.listInput[key].name + ' - ' + this.props.listInput[key].name} />
          </ListItem>
        ])}*/}
          </List>
        </Collapse>
      </List>
    </div>
  );
}}

ListMenu.propTypes = {classes: PropTypes.object.isRequired};
ListMenu = connect(mapStateToProps, mapDispatchToProps)(ListMenu);
export default (withStyles(styles)(ListMenu));
