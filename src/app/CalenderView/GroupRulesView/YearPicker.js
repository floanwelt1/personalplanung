// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';

// Importing components

// const for Material-UI
const styles = theme => ({

});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class YearPicker extends Component {
  constructor(props) {
      super(props);
      this.state = {
        calender: {}
      };
    }

  componentDidMount() {

  }

  placeholderFunction = () => {
    console.log("Fix it!");
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  return (
    <>
      <DatePicker
        openToYearSelection
        className={this.props.class}
        keyboard
        mask={[ /\d/, /\d/, /\d/, /\d/]}
        label="Kalenderjahr"
        format="YYYY"
        value={new Date()}
        onChange={() => this.placeholderFunction()}
        variant="outlined"
      />
    </>
  );
}}

YearPicker.propTypes = {classes: PropTypes.object.isRequired};
YearPicker = connect(mapStateToProps, mapDispatchToProps)(YearPicker);
export default (withStyles(styles)(YearPicker));
