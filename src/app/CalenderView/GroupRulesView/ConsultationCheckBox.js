// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

// Importing components

const styles = theme => ({
  consultationCheckBox: {
    margin: theme.spacing.unit,
    width: 'calc(25% - 16px)'
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ConsultationCheckBox extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <FormControlLabel
      className={classes.consultationCheckBox}
      control={
        <Checkbox checked={this.props.checked} onChange={this.props.handleChange('gilad')} value="gilad" color="primary"/>
      }
      label={this.props.label}
    />
  );
}}

ConsultationCheckBox.propTypes = {classes: PropTypes.object.isRequired};
ConsultationCheckBox = connect(mapStateToProps, mapDispatchToProps)(ConsultationCheckBox);
export default (withStyles(styles)(ConsultationCheckBox));
