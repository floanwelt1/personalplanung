// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

// Importing components

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    },
  formControl: {
    margin: theme.spacing.unit,
    width: 'calc(10% - 16px)',
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class CalenderWeekCheckBox extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
        checked: false,
      };
    }



      handleChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

  return (
    <FormControlLabel
      control={
        <Checkbox checked={this.state.checked} onChange={this.handleChange('jason')} value="jason" />
      }
      label={this.props.label}
    />
  );
}}

CalenderWeekCheckBox.propTypes = {classes: PropTypes.object.isRequired};
CalenderWeekCheckBox = connect(mapStateToProps, mapDispatchToProps)(CalenderWeekCheckBox);
export default (withStyles(styles)(CalenderWeekCheckBox));
