// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

// Importing components

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  list: {
    width: '100%',
    maxHeight: 500,
    position: 'relative',
    overflow: 'auto',
  },

});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ListMenu extends Component {
  constructor(props) {
      super(props);
      this.state = {
        selectedIndexGroup: 0,
        selectedIndexRule: 0,
        groupsOpen: true,
        rulesOpen: true,
      };
  }

  componentDidMount() {

    }

  handleClick = (listKey) => {
    let returnObj = {};
    returnObj[listKey] = !this.state[listKey];
    this.setState(returnObj);

    // this.setState(state => ({ open: !state.open }));
  };

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;
  const leaveKeys = Object.keys(this.props.listInput);

  return (
    <div className={classes.root}>

      <List
        className={classes.list}
        component='nav'
        >
          <ListItem >
            <ListItemText primary="Gruppen" />
          </ListItem>
          <List component="div" disablePadding>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 0}>
              <ListItemText className={classes.listItemText} inset primary="Azubis" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 1}>
              <ListItemText className={classes.listItemText} inset primary="OP" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 2}>
              <ListItemText className={classes.listItemText} inset primary="Praxis" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 3}>
              <ListItemText className={classes.listItemText} inset primary="Buero" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 4}>
              <ListItemText className={classes.listItemText} inset primary="Gruppe A" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexGroup === 5}>
              <ListItemText className={classes.listItemText} inset primary="Gruppe B" />
            </ListItem>
          </List>
        </List>

        <List
          className={classes.list}
          component='nav'
          >
          <ListItem >
            <ListItemText primary="Regeln" />
          </ListItem>
          <List component="div" disablePadding>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexRule === 0}>
              <ListItemText className={classes.listItemText} inset primary="Schule ungerade KW 2019" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexRule === 1}>
              <ListItemText className={classes.listItemText} inset primary="Schule gerade KW 2019" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexRule === 2}>
              <ListItemText className={classes.listItemText} inset primary="Schulferien 2019" />
            </ListItem>
            <Divider />
            <ListItem button className={classes.nested} selected={this.state.selectedIndexRule === 3}>
              <ListItemText className={classes.listItemText} inset primary="Arbeitswochen 2019" />
            </ListItem>
          </List>
          <IconButton className={classes.button} aria-label="Add" color="primary">
            <AddIcon />
          </IconButton>
          <IconButton className={classes.button} aria-label="Delete" color="primary">
            <DeleteIcon />
          </IconButton>
      </List>
    </div>
  );
}}

ListMenu.propTypes = {classes: PropTypes.object.isRequired};
ListMenu = connect(mapStateToProps, mapDispatchToProps)(ListMenu);
export default (withStyles(styles)(ListMenu));
