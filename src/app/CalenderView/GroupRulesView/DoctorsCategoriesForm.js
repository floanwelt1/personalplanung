// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

// Importing components
import ConsultationCheckBox from './ConsultationCheckBox';

const styles = theme => ({
  root: {
    'margin-left': 'auto',
    'margin-right': 'auto',
    width: 'calc(70%)',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class DoctorsCategoriesForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
        key: true
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;
  const consultationsKeys = Object.keys(this.props.consultations);

  return (
    <div className={classes.root}>
      <form noValidate autoComplete="off">
        <FormControl variant="outlined" fullWidth margin='normal'>
          <InputLabel
           ref={ref => {
             this.InputLabelRef = ref;
           }}
           htmlFor={this.props.value}
          >
            Typ
          </InputLabel>
          <Select
            native
            value={this.props.value}
            onChange={this.handleChange(this.props.stateKey)}
            input={
              <OutlinedInput
                name={this.props.name}
                labelWidth={this.state.labelWidth}
                id={this.props.value}
              />
              }
          >
            <option key="2" value="2">Arzt</option>
            <option key="1" value="1">Kategorie</option>
          </Select>
        </FormControl>
        <TextField
           fullWidth
           required
           id="taskName"
           label='Name'
           value={this.props.taskName}
           onChange={this.handleChange('taskName')}
           margin="normal"
           variant="outlined"
           />
      </form>
      <Paper>
      <FormControl fullWidth component="fieldset" margin='normal'>
        <FormLabel component="legend">
          Sprechstunden-Vorlagen
        </FormLabel>
        <FormLabel component="legend">
          Bitte waehlen Sie die Sprechstunden-Vorlagen, die fuer oben gewaehlten
          Arzt/Kategorie verfuegbar sein sollen.
        </FormLabel>
        <FormGroup row>
          {consultationsKeys.map((key) => {
            return (
              <ConsultationCheckBox
              key={key}
              label={this.props.consultations[key].name}
              checked={this.state.key}
              handleChange={(id) => this.handleChange(id)} />
            )
          })}
        </FormGroup>
      </FormControl>
    </Paper>
    <Button variant="outlined" color="primary" className={classes.button}>
      Speichern
    </Button>
    <Button variant="outlined" color="primary" className={classes.button}>
      Aenderungen Verwerfen
    </Button>
  </div>
  );
}}

DoctorsCategoriesForm.propTypes = {classes: PropTypes.object.isRequired};
DoctorsCategoriesForm = connect(mapStateToProps, mapDispatchToProps)(DoctorsCategoriesForm);
export default (withStyles(styles)(DoctorsCategoriesForm));
