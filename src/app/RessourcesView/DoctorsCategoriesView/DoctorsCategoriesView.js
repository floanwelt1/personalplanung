// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import ListMenu from './ListMenu';
import DoctorsCategoriesForm from './DoctorsCategoriesForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks,
    consultations: state.consultations,
    doctorsCategories: state.doctorsCategories
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class DoctorsCategoriesView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <Grid
      container
      className={classes.root}
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      >
        <Grid item xs={12}>
          <h4>Aerzte & Kategorien</h4>
        </Grid>
        <Grid item xs={2}>
          <ListMenu listInput={this.props.doctorsCategories}/>
        </Grid>
        <Grid item xs={10}>
          <DoctorsCategoriesForm consultations={this.props.consultations}/>
        </Grid>
      </Grid>
    );
  }}

DoctorsCategoriesView.propTypes = {classes: PropTypes.object.isRequired};
DoctorsCategoriesView = connect(mapStateToProps, mapDispatchToProps)(DoctorsCategoriesView);
export default (withStyles(styles)(DoctorsCategoriesView));
