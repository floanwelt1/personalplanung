import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  formControl: {
  margin: theme.spacing.unit,
  width: 'calc(100% - 16px)',
},
});

class QualificationSelector extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

    componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = event => {
    this.props.stateChange(this.props.taskKey, event.target.value);
  };

  render () {
  const { classes } = this.props;

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel
       ref={ref => {
         this.InputLabelRef = ref;
       }}
       htmlFor={this.props.value}
     >
      {this.props.name}
     </InputLabel>
     <Select
       native
       value={this.props.value}
       onChange={this.handleChange}
       input={
         <OutlinedInput
         name={this.props.name}
         labelWidth={this.state.labelWidth}
         id={this.props.value}
         />
        }
       >
         <option key="4" value="4">Nicht einsetzbar</option>
         <option key="3" value="3">Schlecht einsetzbar</option>
         <option key="2" value="2">Einsetzbar</option>
         <option key="1" value="1">Bevorzugt einsetzbar</option>
       </Select>
     </FormControl>
    )
  }
}

QualificationSelector.propTypes = {classes: PropTypes.object.isRequired};

export default withStyles(styles)(QualificationSelector);
