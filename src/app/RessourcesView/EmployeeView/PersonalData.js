// Import React
import React, { Component } from 'react';

// Import Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import { DatePicker } from 'material-ui-pickers';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

// const for Material-UI
const styles = theme => ({
  form: {
    textAlign: 'justify'
  },
  textField: {
    width: 'calc(33.333333% - 16px)',
    minWidth: 200,
    margin: theme.spacing.unit,
    textAlign: 'justify'
  },
  formControl: {
    margin: theme.spacing.unit,
    width: '100%',
  },
  groupCheckBox: {
    margin: theme.spacing.unit,
    width: 'calc(16.6% - 16px)'
  },
});


class PersonalData extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  handleChange = key => event => {
    let value = event.target.value;
    this.props.stateChange(key, event.target.value);
  };

  handleGroupChange = name => event => {
    this.props.groupChange(name, event.target.checked)
  };

  handleDateChange = date => {
    this.props.stateChange('dateOfBirth', date);
  }

  render () {
    const { classes } = this.props;

    return (
      <>
      <h4>Persönliche Daten</h4>
      <form  className={classes.form} noValidate autoComplete="off">
        <TextField
        required
        id="firstName"
        label='Vorname'
        value={this.props.firstName}
        className={classes.textField}
        onChange={this.handleChange('firstName')}
        margin="normal"
        variant="outlined"
        />

        <TextField
        required
        id="lastName"
        label='Nachname'
        value={this.props.lastName}
        className={classes.textField}
        onChange={this.handleChange('lastName')}
        margin="normal"
        variant="outlined"
        />

        <DatePicker
        className={classes.textField}
        keyboard
        mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
        label="Geburtsdatum*"
        format="DD.MM.YYYY"
        disableFuture
        openTo="year"
        value={this.props.dateOfBirth}
        onChange={this.handleDateChange}
        margin="normal"
        variant="outlined"
        />
        <br/>
        <TextField
        required
        id="weekHours"
        type="number"
        label='Wochenstunden'
        value={this.props.weekHours}
        className={classes.textField}
        onChange={this.handleChange('weekHours')}
        margin="normal"
        variant="outlined"
        />

        <TextField
        required
        id="leaveDays"
        type="number"
        label='Urlaubstage'
        value={this.props.leaveDays}
        className={classes.textField}
        onChange={this.handleChange('leaveDays')}
        margin="normal"
        variant="outlined"
        />

        <FormControl className={classes.formControl}>
          <FormGroup row>
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.azubis} onChange={this.handleGroupChange('azubis')} value="azubis" color="primary"/>
                }
              label='Azubis'
            />
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.op} onChange={this.handleGroupChange('op')} value="op" color="primary"/>
              }
              label='OP'
            />
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.praxis} onChange={this.handleGroupChange('praxis')} value="praxis" color="primary"/>
              }
              label='Praxis'
            />
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.office} onChange={this.handleGroupChange('office')} value="office" color="primary"/>
              }
              label='Buero'
            />
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.groupA} onChange={this.handleGroupChange('groupA')} value="groupA" color="primary"/>
              }
              label='Gruppe A'
            />
            <FormControlLabel
              className={classes.groupCheckBox}
              control={
                <Checkbox checked={this.props.groupB} onChange={this.handleGroupChange('groupB')} value="groupB" color="primary"/>
              }
              label='Gruppe B'
            />
          </FormGroup>
        </FormControl>
      </form>
    </>
    )
  }
}

PersonalData.propTypes = {classes: PropTypes.object.isRequired};

export default withStyles(styles)(PersonalData);
