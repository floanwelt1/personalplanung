// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

// Importing components
import ControlPanel from './ControlPanel';
import PersonalData from './PersonalData';
import Qualifications from './Qualifications';
import Calender from './Calender';

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: 30,
    },
});

const mapStateToProps = (state) => {
  return {
      employees: state.employees,
      tasks: state.tasks,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class EmployeeView extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
        selectedEmployee: null,
        selectedYear: new Date(),
        _id: null,
        firstName: null,
        lastName: null,
        dateOfBirth: null,
        weekHours: null,
        leaveDays: null,
        qualifications: {},
        groups: {
          azubis: false,
          op: false,
          praxis: false,
          office: false,
          groupA: false,
          groupB: false
        },
        calender: {2019: {
          0: {},
          1: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
              },
          2: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          3: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          4: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          5: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          6: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
              },
          7: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          8: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          9: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
          10: {
            monday: {
              date: new Date(),
              preference: 1
            },
            tuesday: {
              date: new Date(),
              preference: 2
            },
            wednesday: {
              date: new Date(),
              preference: 3
            },
            thursday: {
              date: new Date(),
              preference: 4
            },
            friday: {
              date: new Date(),
              preference: 5
            },
            saturday: {
              date: new Date(),
              preference: 3
            },
            sunday: {
              date: new Date(),
              preference: 2
            },
          },
        }}
      };
    }

    componentDidMount() {

    }

    handleDataChange = (key, value) => {
      let returnObj = {};
      returnObj[key] = value;
      this.setState(returnObj);
    }

    handleGroupChange = (key, value) => {
      let groupState = {...this.state.groups};
      groupState[key] = value;
      this.setState({groups: groupState})
    }

    handleQualificationChange = (key, value) => {
      let qualificationsState = {...this.state.qualifications};
      qualificationsState[key] = value;
      this.setState({qualifications: qualificationsState})
    }

    handlePreferenceChange = (week, day, date, preference) => {
      console.log(week, date, preference);
      let calenderState = {...this.state.calender};
      let dayState = calenderState['2019'][week][day];
      dayState.preference = preference;
      this.setState({calender: calenderState})
    }

    createEmployee = () => {
      return 0;
    }

    selectEmployee = () => {
      return 0;
    }

    saveEmployee = () => {
      return 0;
    }

    deleteEmployee = () => {
      return 0;
    }

    discardChanges = () => {
      return 0;
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <Grid container spacing={24} className={classes.root}  justify="flex-start" alignItems="flex-start">
      <Grid item xs={12}>
        <ControlPanel
          createEmployee={() => this.createEmployee()}
          selectEmployee={() => this.selectEmployee()}
          saveEmployee={() => this.saveEmployee()}
          deleteEmployee={() => this.deleteEmployee()}
          discardChanges={() => this.discardChanges()}
          log={() => console.log(this.state)}
        />
      </Grid>
      <Grid container item xs={12} lg={6} justify="flex-start" alignItems="flex-start">
        <Grid item xs={12}>
          <PersonalData
          firstName={this.state.firstName}
          lastName={this.state.lastName}
          dateOfBirth={this.state.dateOfBirth}
          weekHours={this.state.weekHours}
          leaveDays={this.state.leaveDays}
          groups={this.state.groups}
          stateChange={(key, value) => this.handleDataChange(key, value)}
          groupChange={(key, value) => this.handleGroupChange(key, value)}
          />
        </Grid>
        <Grid item xs={12}>
          <Qualifications
            tasks={this.props.tasks}
            qualifications={this.state.qualifications}
            qualificationChange={(key, value) => this.handleQualificationChange(key, value)}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} lg={6}>
        <Calender
          selectedYear={this.state.selectedYear}
          calender={this.state.calender[2019]}
          yearChange={(key, value) => this.handleDataChange(key, value)}
          preferenceChange={(week, day, date, preference) => this.handlePreferenceChange(week, day, date, preference)}
        />
      </Grid>
    </Grid>
  );
}}

EmployeeView.propTypes = {classes: PropTypes.object.isRequired};
EmployeeView = connect(mapStateToProps, mapDispatchToProps)(EmployeeView);
export default (withStyles(styles)(EmployeeView));
