// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import OutlinedInput from '@material-ui/core/OutlinedInput';

// Importing components

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    },
  button: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 300,
    }
});

const mapStateToProps = (state) => {
  return {
      employees: state.employees,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class ControlPanel extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

    componentDidMount() {
        this.setState({
          labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
        });
      }

    placeholderFunction = () => {
      console.log('fix it');
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <>
      <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel
             ref={ref => {
               this.InputLabelRef = ref;
             }}
             htmlFor='employeeSelector'
           >
             Mitarbeiter
           </InputLabel>
        <Select
          native
          value='to replace with redux'
          onChange={() => this.placeholderFunction()}
          input={
            <OutlinedInput
            name='employeeSelector'
            labelWidth={this.state.labelWidth}
            id= 'employeeSelector'
            />
          }
        >
          <option key="0" value="to replace with redux" />
          <option key="1" value="1">Mitarbeiter1</option>
          <option key="2" value="2">Mitarbeiter2</option>
          <option key="3" value="3">Mitarbeiter3</option>
        </Select>
      </FormControl>
      <Button variant="outlined" color="primary" className={classes.button}>
        Neuen Mitarbeiter erstellen
      </Button>
      <Button variant="outlined" color="primary" className={classes.button}>
        Änderungen Speichern
      </Button>
      <Button variant="outlined" color="primary" className={classes.button}>
        Änderungen Verwerfen
      </Button>
      <Button variant="outlined" color="primary" className={classes.button}>
        Mitarbeiter löschen
      </Button>
      <Button variant="outlined" color="primary" className={classes.button} onClick={() => this.props.log()}>
        Log
      </Button>
    </>
  );
}}

ControlPanel.propTypes = {classes: PropTypes.object.isRequired};
ControlPanel = connect(mapStateToProps, mapDispatchToProps)(ControlPanel);
export default (withStyles(styles)(ControlPanel));
