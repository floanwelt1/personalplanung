// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

// Importing components
import CalenderPreferenceSelector from './CalenderPreferenceSelector';

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    },
  calenderBox: {
    maxHeight: 450,
    overflow: 'auto',
  },
  preferenceSelector: {
    width: 80,
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  weekColumn: {
    width: 20,
    paddingTop: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class PreferenceCalender extends Component {
  constructor(props) {
      super(props);
      this.state = {
        week: {

        }
      };
    }

  componentDidMount() {

  }

  createTableRow = (week, classes) => {
    console.log(week);

    let rowObject = {};
    if (!Object.keys(this.props.calender[week]).length) {
      return null;
    } else {
      rowObject = this.props.calender[week];
    }
    console.log(this.props.calender);
    console.log(rowObject);

    // let rowObject = {};
    // console.log(new Date());
    // if (!Object.keys(this.state.week[week]).length) {
    //   return null;
    // } else {
    //
    //   rowObject = this.state.week[week];
    // }


    return(
      <TableRow>
        <TableCell component="th" scope="row" className={classes.weekColumn}>{week}</TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.monday.date.toDateString()} value={rowObject.monday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'monday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.tuesday.date.toDateString()} value={rowObject.tuesday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'tuesday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.wednesday.date.toDateString()} value={rowObject.wednesday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'wednesday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.thursday.date.toDateString()} value={rowObject.thursday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'thursday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.friday.date.toDateString()} value={rowObject.friday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'friday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.saturday.date.toDateString()} value={rowObject.saturday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'saturday', date, preference)} /></TableCell>
        <TableCell align="center" className={classes.preferenceSelector}><CalenderPreferenceSelector label={rowObject.sunday.date.toDateString()} value={rowObject.sunday.preference} preferenceChange={(date, preference) => this.props.preferenceChange(week, 'sunday', date, preference)} /></TableCell>
      </TableRow>
    )
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;
    const weekKeys = Object.keys(this.props.calender);

    return (
      <Paper className={classes.calenderBox}>
        <Table padding='none'>
          <TableHead>
            <TableRow>
              <TableCell className={classes.weekColumn}>KW</TableCell>
              <TableCell align="center">Montag</TableCell>
              <TableCell align="center">Dienstag</TableCell>
              <TableCell align="center">Mittwoch</TableCell>
              <TableCell align="center">Donnerstag</TableCell>
              <TableCell align="center">Freitag</TableCell>
              <TableCell align="center">Samstag</TableCell>
              <TableCell align="center">Sonntag</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {weekKeys.map(key => this.createTableRow(key, classes))}
          </TableBody>
        </Table>
      </Paper>
  );
}}

PreferenceCalender.propTypes = {classes: PropTypes.object.isRequired};
export default (withStyles(styles)(PreferenceCalender));
