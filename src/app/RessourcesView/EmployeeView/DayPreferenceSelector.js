// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

// Importing components

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    },
  formControl: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    width: 'calc(14.25% - 16px)',
    minWidth: 100
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class DayPreferenceSelector extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

    componentDidMount() {
        this.setState({
          labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
        });
      }

      handleChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel
             ref={ref => {
               this.InputLabelRef = ref;
             }}
             htmlFor={this.props.value}
             >
               {this.props.label}
             </InputLabel>
             <Select
             native
             value={this.props.value}
             onChange={this.handleChange(this.props.stateKey)}
             input={
               <OutlinedInput
               name={this.props.name}
               labelWidth={this.state.labelWidth}
               id= {this.props.value}
               />
             }
               >
               <option value={1}>Egal</option>
               <option value={2}>Nein</option>
               <option value={3}>Früh</option>
               <option value={4}>Spät</option>
               <option value={5}>Ganz</option>
             </Select>
           </FormControl>
  );
}}

DayPreferenceSelector.propTypes = {classes: PropTypes.object.isRequired};
DayPreferenceSelector = connect(mapStateToProps, mapDispatchToProps)(DayPreferenceSelector);
export default (withStyles(styles)(DayPreferenceSelector));
