// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

// Importing components

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class CalenderPreferenceSelector extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

    componentDidMount() {

      }

      handleChange = name => event => {
        this.props.preferenceChange(this.props.label, event.target.value);
    };


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

  return (
    <FormControl fullWidth>
    {this.props.label}
      <NativeSelect
        value={this.props.value}
        onChange={this.handleChange()}
        name={this.props.name}
      >
        <option value={1}>Egal</option>
        <option value={2}>Nein</option>
        <option value={3}>Früh</option>
        <option value={4}>Spät</option>
        <option value={5}>Ganz</option>
      </NativeSelect>
     </FormControl>
  );
}}

CalenderPreferenceSelector.propTypes = {classes: PropTypes.object.isRequired};
CalenderPreferenceSelector = connect(mapStateToProps, mapDispatchToProps)(CalenderPreferenceSelector);
export default (withStyles(styles)(CalenderPreferenceSelector));
