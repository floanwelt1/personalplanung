// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

// Importing components
import DayPreferenceSelector from './DayPreferenceSelector';
import CalenderWeekCheckbox from './CalenderWeekCheckbox';

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit,
    },
  expansionPanelDetails: {
    padding: theme.spacing.unit,
  },
  formControl: {
    margin: theme.spacing.unit,
    },
  button: {
    margin: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2,
  },
});



class CreateRule extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {

  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <ExpansionPanel className={classes.root}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>Regel erstellen</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.expansionPanelDetails}>
      <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={24}>
          <Grid item xs={12}>
            <FormLabel component="legend">Präferenzen</FormLabel>
            <DayPreferenceSelector label='Montag' />
            <DayPreferenceSelector label='Dienstag' />
            <DayPreferenceSelector label='Mittwoch' />
            <DayPreferenceSelector label='Donnerstag' />
            <DayPreferenceSelector label='Freitag' />
            <DayPreferenceSelector label='Samtag' />
            <DayPreferenceSelector label='Sonntag' />
          </Grid>
          <Grid item xs={12}>
            <FormControl component="fieldset" className={classes.formControl}>
            <FormLabel component="legend">Kalenderwochen</FormLabel>
              <Table padding='checkbox'>
                <TableBody>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='01' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='02' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='03' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='04' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='05' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='06' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='07' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='08' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='09' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='10' /></TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='11' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='12' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='13' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='14' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='15' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='16' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='17' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='18' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='19' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='20' /></TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='21' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='22' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='23' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='24' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='25' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='26' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='27' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='28' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='29' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='30' /></TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='31' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='32' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='33' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='34' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='35' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='36' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='37' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='38' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='39' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='40' /></TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='41' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='42' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='43' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='44' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='45' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='46' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='47' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='48' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='49' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='50' /></TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center"><CalenderWeekCheckbox label='51' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='52' /></TableCell>
                      <TableCell align="center"><CalenderWeekCheckbox label='53' /></TableCell>
                    </TableRow>
                </TableBody>
              </Table>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button variant="outlined" color="primary" className={classes.button}>
              Waehle alle geraden
            </Button>
            <Button variant="outlined" color="primary" className={classes.button}>
              Waehle alle ungeraden
            </Button>
            <Button variant="outlined" color="primary" className={classes.button}>
              Alle abwaehlen
            </Button>
            <Button variant="outlined" color="primary" className={classes.button}>
              Regel anwenden
            </Button>
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}}

CreateRule.propTypes = {classes: PropTypes.object.isRequired};
export default (withStyles(styles)(CreateRule));
