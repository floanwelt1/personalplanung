// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import QualificationSelector from './QualificationSelector';

// const for Material-UI
const styles = theme => (
  {
  root: {
    flexGrow: 1,
    },
  }
);

class Qualifications extends Component {
  constructor(props) {
      super(props);
      this.state = {
        qualifications: {},
      };
    }

    componentDidMount() {

      }

  renderQualificationFields = (key) => {
    let qualification = this.props.tasks[key];
    let value = 4;
    if (this.props.qualifications[qualification.key]) {
      value = this.props.qualifications[qualification.key];
    }
    return (
      <Grid item key={key} xs={12} sm={6} md={4} lg={3} xl={2}>
        <QualificationSelector
          key={key}
          taskKey={key}
          stateChange={(key, value) => {this.props.qualificationChange(key, value)}}
          name={qualification.name}
          value={value}
        />
      </Grid>
      )
    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;
    let keysTasks = Object.keys(this.props.tasks);

    return (
      <>
        <h4>Einsetzbarkeiten</h4>
        {/* Grid einpflegen */}
        <Grid container spacing={0} className={classes.root}  justify="flex-start" alignItems="flex-start">
          {keysTasks.map((key) => this.renderQualificationFields(key))}
        </Grid>
      </>
    );
}}

Qualifications.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(Qualifications));
