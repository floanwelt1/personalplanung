// Importing React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

// Importing components
import ConsultationsTasksTable from './ConsultationsTasksTable';
import AddTaskForm from './AddTaskForm';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ConsultationsForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

  componentDidMount() {
      this.setState({
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      });
    }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };
//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <div className={classes.root}>
      <form noValidate autoComplete="off">
        <TextField
        fullWidth
        required
        id="taskName"
        label='Name'
        value={this.props.taskName}
        onChange={this.handleChange('taskName')}
        margin="normal"
        variant="outlined"
        />
        <TextField
        fullWidth
        required
        id="taskName"
        label='Beschreibung'
        value={this.props.taskName}
        onChange={this.handleChange('taskName')}
        margin="normal"
        variant="outlined"
        />
        <FormControl variant="outlined" fullWidth margin='normal'>
          <InputLabel
             ref={ref => {
               this.InputLabelRef = ref;
             }}
             htmlFor={this.props.value}
           >
             Typ
           </InputLabel>
           <Select
             native
             value={this.props.value}
             onChange={this.handleChange(this.props.stateKey)}
             input={
               <OutlinedInput
               name={this.props.name}
               labelWidth={this.state.labelWidth}
               id={this.props.value}
               />
             }
             >
               <option key="2" value="2">Arzt</option>
               <option key="1" value="1">Sonstiges</option>
           </Select>
         </FormControl>
         <FormGroup row>
           <FormControlLabel
            control={
            <Switch
              checked={this.state.checkedB}
              onChange={this.handleChange('checkedB')}
              value="checkedB"
              color="primary"
            />
            }
            label="Ganztaegig"
            />
        </FormGroup>
      </form>
      <AddTaskForm />
      <ConsultationsTasksTable />
      <br/>
      <Button variant="outlined" color="primary" className={classes.button}>
        Vorlage Speichern
      </Button>
      <Button variant="outlined" color="primary" className={classes.button}>
        Aenderungen Verwerfen
      </Button>
    </div>
  );
}}

ConsultationsForm.propTypes = {classes: PropTypes.object.isRequired};
ConsultationsForm = connect(mapStateToProps, mapDispatchToProps)(ConsultationsForm);
export default (withStyles(styles)(ConsultationsForm));
