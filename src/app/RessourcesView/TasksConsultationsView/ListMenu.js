// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';

// Importing components

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit,
  },
  list: {
    width: '100%',
    height: window.innerHeight - (48 + 48 + 61 + 90),
    minHeight: 600,
    maxHeight: 1200,
    position: 'relative',
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ListMenu extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

  componentDidMount() {

  }

  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  const listKeys = Object.keys(this.props.listInput);
  return (
    <div className={classes.root}>
      <List className={classes.list} component='nav'>
        {listKeys.map(key => (
          <ListItem
          key={key}
          button
          selected={this.state.selectedIndex === key}
          onClick={event => this.handleListItemClick(event, key)}
          >
            <ListItemText primary={this.props.listInput[key].name} />
          </ListItem>
        ))}
      </List>
      <IconButton className={classes.button} aria-label="Add" color="primary">
        <AddIcon />
      </IconButton>
      <IconButton className={classes.button} aria-label="Delete" color="primary">
        <DeleteIcon />
      </IconButton>
    </div>
  );
}}

ListMenu.propTypes = {classes: PropTypes.object.isRequired};
ListMenu = connect(mapStateToProps, mapDispatchToProps)(ListMenu);
export default (withStyles(styles)(ListMenu));
