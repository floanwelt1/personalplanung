// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import TasksView from './TasksView';
import ConsultationsView from './ConsultationsView';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class TasksConsultationsView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  componentDidMount() {

    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <Grid
    container
    className={classes.root}
    direction="row"
    justify="flex-start"
    alignItems="flex-start"
    spacing={24}>
      <Grid item xs={5}>
          <TasksView />
      </Grid>
      <Grid item xs={7}>
        <ConsultationsView />
      </Grid>
    </Grid>
  );
}}

TasksConsultationsView.propTypes = {classes: PropTypes.object.isRequired};
TasksConsultationsView = connect(mapStateToProps, mapDispatchToProps)(TasksConsultationsView);
export default (withStyles(styles)(TasksConsultationsView));
