// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import ListMenu from './ListMenu';
import ConsultationsForm from './ConsultationsForm';

const styles = theme => ({

});

const mapStateToProps = (state) => {
  return {
      consultations: state.consultations,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ConsultationsView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  componentDidMount() {

    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

  return (
    <Grid
    container
    direction="row"
    justify="flex-start"
    alignItems="flex-start"
    >
      <Grid item xs={12}>
        <h4>Sprechstunden-Vorlagen</h4>
      </Grid>
      <Grid item xs={3}>
        <ListMenu listInput={this.props.consultations}/>
      </Grid>
      <Grid item xs={9}>
        <ConsultationsForm />
      </Grid>
    </Grid>
  );
}}

ConsultationsView.propTypes = {classes: PropTypes.object.isRequired};
ConsultationsView = connect(mapStateToProps, mapDispatchToProps)(ConsultationsView);
export default (withStyles(styles)(ConsultationsView));
