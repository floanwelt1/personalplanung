// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormLabel from '@material-ui/core/FormLabel';

// Importing components

const styles = theme => ({
  table: {
    'margin-left': 'auto',
    'margin-right': 'auto',
    maxHeight: 350,
    overflow: 'auto',
  },
  buttonCell: {
    width: 88,
    padding: 0,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class ConsultationsTasksTable extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  componentDidMount() {

    }

  createTableRow = (taskName, start = 0, end = 0) => {
    return(
      <TableRow>
        <TableCell className={this.props.classes.buttonCell}>
        <IconButton aria-label="Edit">
          <EditIcon fontSize="small" />
        </IconButton>
          <IconButton aria-label="Delete">
            <DeleteIcon fontSize="small" />
          </IconButton>
        </TableCell>
        <TableCell align="center">
          {taskName}
        </TableCell>
        <TableCell align="center">{start}</TableCell>
        <TableCell align="center">{end}</TableCell>
        </TableRow>
    )
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <Paper className={classes.table}>
      <FormLabel component="legend">
        Zugehoerige Taetigkeiten
      </FormLabel>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="none" align="left" className={classes.buttonCell}></TableCell>
            <TableCell align="center">Taetigkeit</TableCell>
            <TableCell align="center">Anfang</TableCell>
            <TableCell align="center">Ende</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {this.createTableRow("VU")}
            {this.createTableRow("VU", "+30", "-30")}
            {this.createTableRow("VU", "+30", "-30")}
            {this.createTableRow("Tropfer", "+30", "-30")}
            {this.createTableRow("CV", "+30", "-30")}
            {this.createTableRow("Schreiber", "0", "0")}
            {this.createTableRow("Zuschauer", "+60", "-30")}
            {this.createTableRow("Mitlaeufer", "+30", "-30")}
        </TableBody>
      </Table>
    </Paper>
  );
}}

ConsultationsTasksTable.propTypes = {classes: PropTypes.object.isRequired};
ConsultationsTasksTable = connect(mapStateToProps, mapDispatchToProps)(ConsultationsTasksTable);
export default (withStyles(styles)(ConsultationsTasksTable));
