// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

// Importing components
import ListMenu from './ListMenu';
import TasksForm from './TasksForm';

const styles = theme => ({

});

const mapStateToProps = (state) => {
  return {
      tasks: state.tasks,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class TasksView extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
    }

  componentDidMount() {

    }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

  return (
    <Grid
    container
    direction="row"
    justify="flex-start"
    alignItems="flex-start"
    >
      <Grid item xs={12}>
        <h4>Taetigkeiten</h4>
      </Grid>
      <Grid item xs={4}>
        <ListMenu listInput={this.props.tasks}/>
      </Grid>
      <Grid item xs={8}>
        <TasksForm />
    </Grid>
  </Grid>
  );
}}

TasksView.propTypes = {classes: PropTypes.object.isRequired};
TasksView = connect(mapStateToProps, mapDispatchToProps)(TasksView);
export default (withStyles(styles)(TasksView));
