// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

// Importing components
import EmployeeView from './EmployeeView/EmployeeView';
import TasksConsultationsView from './TasksConsultationsView/TasksConsultationsView';
import DoctorsCategoriesView from './DoctorsCategoriesView/DoctorsCategoriesView';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

function RessourcesContainer(props) {
  switch(props.view) {
    case 'employees':
    return (<EmployeeView />)
    case 'tasks_consultations':
    return(<TasksConsultationsView />)
    case 'doctors_categories':
    return (<DoctorsCategoriesView />)
    default:
    break;
  }
}

class RessourcesView extends Component {
  constructor(props) {
      super(props);
      this.state = {
        currentRessource: 'employees'
      };
    }

  changeRessource = (event, value) => {
    this.setState({currentRessource: value})
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {

  return (
    <div>
      <AppBar position="static">
        <Tabs value={this.state.currentRessource} onChange={this.changeRessource} fullWidth>
          <Tab value={'employees'} label={'Mitarbeiter'} />
          <Tab value={'tasks_consultations'} label={'Tätigkeiten & Sprechstundenvorlagen'} />
          <Tab value={'doctors_categories'} label={'Ärzte & Kategorien'} />
        </Tabs>
      </AppBar>
      <RessourcesContainer view={this.state.currentRessource} />
    </div>
  );
}}

RessourcesView.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(RessourcesView));
