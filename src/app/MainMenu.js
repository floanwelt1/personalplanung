// Main Menu for the App
// This gets imported by App.js

// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions
import { setMainView } from "../Redux/actions/mainActions";

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuIcon from '@material-ui/icons/Menu';

// const for Material-UI settings
const styles = theme => ({
  root: {
    display: 'flex',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {
    setMainView: (viewName) => {
      dispatch(setMainView(viewName));
    }
  };
};

class MainMenu extends Component {
// Setting state for the Menu
// This is pretty basic stuff from the Material-UI examples, so I will not comment further
// If you need more information regardless, hit me up and I'll explain further
  constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        };
      }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (mainView) => {
    this.setState({ anchorEl: null });
    if (typeof mainView === 'string') {
      this.props.setMainView(mainView);
    }
  };



  render () {
    const { anchorEl } = this.state;
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton className={classes.menuButton} color="inherit"
              aria-label="Menu" aria-owns={anchorEl ? 'simple-menu' : null}
              aria-haspopup="true" onClick={this.handleClick}>
            <MenuIcon />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}>
              <MenuItem onClick={(mainView) => { this.handleClose('planningView') }}>Dienstplanung</MenuItem>
              <MenuItem onClick={(mainView) => { this.handleClose('ressourcesView') }}>Ressourcenverwaltung</MenuItem>
              <MenuItem onClick={(mainView) => { this.handleClose('calenderView') }}>Kalender</MenuItem>
              <MenuItem onClick={(mainView) => { this.handleClose('exportView') }}>Export</MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Personalplanung
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

MainMenu.propTypes = {classes: PropTypes.object.isRequired};
MainMenu = connect(mapStateToProps, mapDispatchToProps)(MainMenu);
export default withStyles(styles)(MainMenu);
